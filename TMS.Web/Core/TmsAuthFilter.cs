﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using TMS.Web.Enums;

namespace TMS.Web.Core
{
    public class TmsAuthFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var userid = context.HttpContext.Request.Headers[HttpHeaders.HeaderUserid.ToString()].FirstOrDefault();
            var companyid = context.HttpContext.Request.Headers[HttpHeaders.HeaderCompanyid.ToString()].FirstOrDefault();
            var branchid = context.HttpContext.Request.Headers[HttpHeaders.HeaderBranchid .ToString()].FirstOrDefault();
            var authtoken = context.HttpContext.Request.Headers[HttpHeaders.HeaderAuthtoken.ToString()].FirstOrDefault();

            try
            {
                // our custom logic
            }
            catch (Exception ex)
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }
}
