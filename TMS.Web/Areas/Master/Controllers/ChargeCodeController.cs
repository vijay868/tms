﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TMS.Master.BusinessFactory;
using TMS.Web.Core;

namespace TMS.Web.Areas.Master.Controllers
{
    [Area("master")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    [TmsAuthFilter]
    public class ChargeCodeController : ControllerBase
    {
        private ChargeCodeBO chargeCodeBO;
        public ChargeCodeController(ChargeCodeBO _chargeCodeBO) => chargeCodeBO = _chargeCodeBO;

        [HttpGet, Route("chargecodelist")]
        public IActionResult ChargeCodelist()
        {
            return Ok(chargeCodeBO.GetChargeCodeList().ToList());
        }

    }
}