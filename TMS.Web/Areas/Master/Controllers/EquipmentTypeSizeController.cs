﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TMS.Master.BusinessFactory;
using TMS.Master.Contract;
using TMS.Web.Core;

namespace TMS.Web.Areas.Master.Controllers
{
    [Area("master")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    [TmsAuthFilter]
    public class EquipmentTypeSizeController : ControllerBase
    {
        private EquipmentTypeSizeBO equipmentTypeSizeBO;
        public EquipmentTypeSizeController(EquipmentTypeSizeBO _equipmentTypeSizeBO) => equipmentTypeSizeBO = _equipmentTypeSizeBO;

        [HttpGet, Route("sizelist")]
        public IActionResult SizeList()
        {
            return Ok(equipmentTypeSizeBO.GetEquipmentTypeSizeList().Select
                                (s => new { code = s.EquipmentSize, desc= s.EquipmentSize }).Distinct().ToList());
        }

        [HttpGet, Route("typelist")]
        public IActionResult TypeList()
        {
            return Ok(equipmentTypeSizeBO.GetEquipmentTypeSizeList().Select
                                (s => new { code = s.EquipmentType, desc = s.EquipmentType }).Distinct().ToList());
        }
    }
}