﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TMS.Master.BusinessFactory;
using TMS.Web.Core;

namespace TMS.Web.Areas.Master.Controllers
{
    [Route("api/[area]/[controller]")]
    [Area("master")]
    [ApiController]
    [TmsAuthFilter]
    public class CustomerController : ControllerBase
    {
        private CustomerBO customerBO;
        public CustomerController(CustomerBO _customerBO) => customerBO = _customerBO;

        [HttpGet]
        public IActionResult Search([FromQuery(Name = "code")] string code, [FromQuery(Name = "name")] string name)
        {
            if(name != null)
                return Ok(customerBO.PerformSearchWrapper(code, name));
            return Ok();
        }

        [HttpGet, Route("{code}/{name?}")]
        public IActionResult AutoComplete(string code, string name = "")
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                return Ok(customerBO.PerformSearchWrapper(code, name));
            }
            return Ok();            
        }

    }    
}