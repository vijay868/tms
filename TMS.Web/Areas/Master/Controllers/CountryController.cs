﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

using TMS.Master.BusinessFactory;
using TMS.Master.Contract;

namespace TMS.Web.Areas.Master
{
    [Area("master")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        private CountryBO countryBO;
        public CountryController(CountryBO _countryBO) => countryBO = _countryBO;
        

        [HttpGet, Route("list")]
        public IEnumerable<Country> List() => countryBO.GetList();
        
    }
}