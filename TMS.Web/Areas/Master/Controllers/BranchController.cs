﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TMS.Master.BusinessFactory;
using TMS.Master.Contract;

namespace TMS.Web.Areas.Master.Controllers
{
    [Area("master")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class BranchController : ControllerBase
    {
        private BranchBO branchBO;
        public BranchController(BranchBO _branchBO) => branchBO = _branchBO;

        [HttpGet, Route("{branchCode}")]
        public Branch GetBranch(string branchCode) => branchBO.GetBranch(new Branch { BranchCode = branchCode });

        [HttpGet, Route("list/{companyCode}")]
        public List<Branch> GetList(string companyCode) => branchBO.GetBranchList(companyCode);
    }
}