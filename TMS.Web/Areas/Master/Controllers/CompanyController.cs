﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TMS.Master.BusinessFactory;
using TMS.Master.Contract;

namespace TMS.Web.Areas.Master.Controllers
{
    [Area("master")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private CompanyBO companyBO;
        public CompanyController(CompanyBO _companyBO) => companyBO = _companyBO;

        [HttpGet, Route("{companyCode}")]
        public Company GetCompany(string companyCode) => companyBO.GetCompany(new Company { CompanyCode = companyCode });
    }
}