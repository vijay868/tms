﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TMS.Master.BusinessFactory;
using TMS.Master.Contract;
using TMS.Web.Core;



namespace TMS.Web.Areas.Master.Controllers
{
    [Area("master")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    [TmsAuthFilter]
    public class LookUpController : ControllerBase
    {
        private LookupBO lookupBO;
        public LookUpController(LookupBO _lookupBO) => lookupBO = _lookupBO;

        [HttpGet, Route("list")]
        [ResponseCache(Duration = 10000)]
        public IActionResult Index() => Ok(lookupBO.GetLookupList());      

        
    }
}