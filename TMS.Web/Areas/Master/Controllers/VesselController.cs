﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TMS.Master.BusinessFactory;
using TMS.Web.Core;

namespace TMS.Web.Areas.Master.Controllers
{
    [Area("master")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    [TmsAuthFilter]
    public class VesselController : ControllerBase
    {
        private VesselBO vesselBO;
        public VesselController(VesselBO _vesselBO) => vesselBO = _vesselBO;

        [HttpGet, Route("vessellist")]
        public IActionResult VesselList()
        {
            return Ok(vesselBO.GetVesselList().ToList());
        }
    }
}