﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TMS.Master.BusinessFactory;
using TMS.Master.Contract.Dto;
using TMS.Web.Core;

namespace TMS.Web.Areas.Master.Controllers
{
    [Route("api/[area]/[controller]")]
    [Area("master")]
    [ApiController]
    [TmsAuthFilter]
    public class PortController : ControllerBase
    {
        private PortBO portBO;
        public PortController(PortBO _portBO) => portBO = _portBO;

        [HttpGet, Route("{portName}")]
        public List<PortDto> PortSearch(string portName) => portBO.PerformSearchWrapper(portName);

    }
}