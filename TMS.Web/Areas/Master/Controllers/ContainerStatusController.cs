﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TMS.Master.BusinessFactory;
using TMS.Master.Contract;
using TMS.Web.Core;

namespace TMS.Web.Areas.Master.Controllers
{
    [Area("master")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    [TmsAuthFilter]
    public class ContainerStatusController : ControllerBase
    {
        private ContainerStatusBO containerStatusBO;
        public ContainerStatusController(ContainerStatusBO _containerStatusBO) => containerStatusBO = _containerStatusBO;

        [HttpGet, Route("containerstatuslist")]
        public IActionResult ContainerStatuslist()
        {
            return Ok(containerStatusBO.GetList().ToList());
        }

        [HttpPost, Route("save")]
        private IActionResult SaveContainerStatus(ContainerStatus containerStatus)
        {
            var result = containerStatusBO.SaveContainerStatus(new ContainerStatus
            {
                Code = containerStatus.Code,
                Description = containerStatus.Description,
                CreatedBy = "",
                ModifiedBy = "",
                IsHold = containerStatus.IsHold,
                Status = true
            });

            if (result)
            {
                return Ok(containerStatusBO.GetList().ToList());
            }
            return null;
        }

        [HttpGet, Route("getcontainerStatusItem/{code}")]
        private IActionResult GetContainerStatusItem(string code)
        {
            ContainerStatus containerstatus = containerStatusBO.GetContainerStatus(new ContainerStatus { Code = code });

            if (containerstatus == null)
            {
                return null;
            }
            else
            {
                return Ok(containerstatus);
            }
        }
    }
}