﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TMS.Web.Areas.Auth.Dtos
{
    public class LoginDTO
    {
        public string email { get; set; }

        public string password { get; set; }
    }
}
