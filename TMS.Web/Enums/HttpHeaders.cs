﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TMS.Web.Enums
{
    public enum HttpHeaders
    {
        HeaderUserid,
        HeaderCompanyid,
        HeaderBranchid,
        HeaderAuthtoken
    }
}
