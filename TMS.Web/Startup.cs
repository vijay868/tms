using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Http.Extensions;
using System;
using Microsoft.Net.Http.Headers;

using Autofac;
using Autofac.Extensions.DependencyInjection;
using TMS.ServiceContainer;
using TMS.Common;
using Microsoft.AspNetCore.Http;

namespace TMS.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<DataBaseConfiguration>(Configuration.GetSection("ConnectionStrings"));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            services.AddResponseCaching();
            services.AddResponseCompression();
            services.AddOptions();
            services.AddCors();


            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterModule<MasterModule>();


            ApplicationContainer = builder.Build();
            return new AutofacServiceProvider(ApplicationContainer);
        }
        // ref: https://asp.net-hacker.rocks/2017/05/08/add-custom-ioc-in-aspnetcore.html
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseResponseCaching();
            /*
            app.Run(async (context) =>
            {
                context.Response.GetTypedHeaders().CacheControl = new CacheControlHeaderValue()
                {
                    Public = true,
                    MaxAge = TimeSpan.FromSeconds(10)
                };
                context.Response.Headers[HeaderNames.Vary] = new string[] { "Accept-Encoding" };

                await context.Response.WriteAsync("Hello World! " + DateTime.UtcNow);
            });
            */

            
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseCors(
                options => options.WithOrigins("http://localhost:4200")
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials()
             );

            app.UseMvc(routes =>
            {
                //routes.MapRoute(
                //    "master_route",
                //    "Manage/api/{controller}/{action}/{id?}",
                //    defaults: new
                //    {
                //        area = "master"
                //    },
                //    constraints: new
                //    {
                //        area = "master"
                //    }
                //);

                routes.MapRoute(
                    name: "default",
                    template: "api/{area}/{controller}/{action=Index}/{id?}"
                );
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });


        }
    }
}

// ref https://stackoverflow.com/questions/44379560/how-to-enable-cors-in-asp-net-core-webapi
