﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Common;

namespace TMS.Master.Contract
{
    public class EquipmentTypeSizeMaster : IContract
    {
        // Constructor 
        public EquipmentTypeSizeMaster() { }

        // Public Members 

        public string EquipmentSize { get; set; }

        public string EquipmentType { get; set; }

        public string EquipmentTypeSize { get; set; }

        public string ISOCode { get; set; }

        public string MappingCode { get; set; }

        public string Description { get; set; }

        public decimal TareWeight { get; set; }

        public decimal GrossWeight { get; set; }

        public decimal MaxGrossWeight { get; set; }


        public bool IsContainer { get; set; }

        public bool IsReefer { get; set; }

        public bool IsTruck { get; set; }

        public Int16 TEU { get; set; }

        public Int16 Height { get; set; }

        public string HeightDescription { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public bool Status { get; set; }
    }
}
