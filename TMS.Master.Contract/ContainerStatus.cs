﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Common;

namespace TMS.Master.Contract
{
    public class ContainerStatus : IContract
    {
        // Constructor 
        public ContainerStatus() { }

        // Public Members 

        public string Code { get; set; }

        public string Description { get; set; }

        public bool IsHold { get; set; }

        public bool Status { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }


    }
}
