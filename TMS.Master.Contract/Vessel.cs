﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Common;

namespace TMS.Master.Contract
{
    public class Vessel : IContract
    {
        // Constructor 
        public Vessel() { }

        // Public Members 

        public string VesselCode { get; set; }

        public string VesselName { get; set; }

        public string CallSign { get; set; }

        public string MappingCode { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public bool Status { get; set; }


    }
}
