﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Common;

namespace TMS.Master.Contract
{
    public class ContainerFixPort : IContract
    {
        // Constructor 
        public ContainerFixPort() { }

        // Public Members 

        public string ContainerNo { get; set; }

        public string PortCode { get; set; }

        public string PortName { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }


    }
}
