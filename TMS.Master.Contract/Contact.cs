﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Common;

namespace TMS.Master.Contract
{
    public class Contact: IContract
    {
        public Contact() { }

        // Public Members 

        public Int64 ContactID { get; set; }

        public string AddressLinkID { get; set; }

        public string EntityType { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string State { get; set; }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string PostCode { get; set; }

        public string PhoneNumber { get; set; }

        public string FaxNumber { get; set; }

        public string ContactPerson { get; set; }

        public string MobilePhoneNumber { get; set; }

        public string EmailID { get; set; }

        public string WebSite { get; set; }

        public string Social1 { get; set; }

        public string Social2 { get; set; }

        public bool IsDefault { get; set; }

        public bool Status { get; set; }

        public bool IsNewRecord { get; set; }

        public bool Selected { get; set; }
    }
}
