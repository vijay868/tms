﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Common;

namespace TMS.Master.Contract
{
    public class Customer : IContract
    {
        public Customer() { }

        // Public Members 

        public string CustomerCode { get; set; }

        public string CustomerName { get; set; }
        public string CustomerName2 { get; set; }



        public string RegistrationNo { get; set; }

        public bool IsBillingCustomer { get; set; }

        public bool IsShipper { get; set; }

        public bool IsConsignee { get; set; }

        public bool IsFwdAgent { get; set; }

        public bool IsShippingAgent { get; set; }

        public bool IsHaulier { get; set; }

        public bool IsTransporter { get; set; }

        public string OperatorCode { get; set; }

        public string CurrencyCode { get; set; }

        public string DebtorCode { get; set; }

        public string RevenueAccount { get; set; }

        public bool Status { get; set; }

        public string Remark { get; set; }

        public Int16 NumberOfLongStandingDays { get; set; }

        public Int16 CreditTerm { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string AuditID { get; set; }

        public bool IsNewRecord { get; set; }

        private List<Contact> contactItems = new List<Contact>();

        public List<Contact> ContactItems
        {
            get { return contactItems; }
            set { contactItems = value; }
        }
    }
}
