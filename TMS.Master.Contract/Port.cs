﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Common;

namespace TMS.Master.Contract
{
    public class Port : IContract
    {
        // Constructor 
        public Port() { }

        // Public Members 

        public string PortAreaCode { get; set; }

        public string PortAreaName { get; set; }

        public string CountryCode { get; set; }

        public Int16 AreaPortType { get; set; }

        public string PostCode { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public bool Status { get; set; }

        public string ISOCode { get; set; }

        public string MappingCode { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string AuditID { get; set; }

        public Int16 TradeMode { get; set; }

        public string PaperLessCode { get; set; }

        public string TradeModeDescription { get; set; }

        public string AreaPortTypeDescription { get; set; }



    }
}
