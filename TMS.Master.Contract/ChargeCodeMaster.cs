﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Common;

namespace TMS.Master.Contract
{
    public class ChargeCodeMaster : IContract
    {
        // Constructor 
        public ChargeCodeMaster() { }

        // Public Members 

        public string ChargeCode { get; set; }

        public string Description { get; set; }

        public Int16 ChargeType { get; set; }

        public Int16 BillingUnit { get; set; }

        public bool Status { get; set; }

        public decimal VAT { get; set; }

        public decimal CreditTerm { get; set; }


        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string AuditID { get; set; }

        public string LookupModuleDescription { get; set; }

        public string BillingUnitDescription { get; set; }

        public string ChargeTypeICDDescription { get; set; }

        public string ChargeTypeTruckingDescription { get; set; }

        public string ChargeTypeDescription { get; set; }

        public Int16 Module { get; set; }

        public Int16 PaymentTerm { get; set; }

        public string PaymentTermDescription { get; set; }


        public Int16 ChargeTerm { get; set; }

        public string ChargeTermDescription { get; set; }

        public bool IsByService { get; set; }
    }
}
