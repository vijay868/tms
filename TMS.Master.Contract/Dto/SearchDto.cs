﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TMS.Master.Contract.Dto
{
    public class SearchDto
    {
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
    }
}
