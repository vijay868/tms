﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TMS.Master.Contract.Dto
{
    public class LookUpDto
    {
        public Int16? LookupID { get; set; }

        public string LookupCode { get; set; }

        public string LookupDescription { get; set; }

        public string LookupCategory { get; set; }
    }
}
