﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TMS.Master.Contract.Dto
{
    public class PortDto
    {
        public string PortAreaCode { get; set; }

        public string PortAreaName { get; set; }
    }
}
