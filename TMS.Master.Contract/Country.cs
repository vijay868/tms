﻿using System;
using TMS.Common;

namespace TMS.Master.Contract
{
    public class Country : IContract
    {
        public Country() { }

        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        public string EDIMapping { get; set; }

        public string CreatedBy { get; set; }


        public DateTime CreatedOn { get; set; }

        public string ModifiedBy { get; set; }


        public DateTime ModifiedOn { get; set; }

        public string DailingCode { get; set; }


    }
}
