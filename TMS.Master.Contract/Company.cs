﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Common;

namespace TMS.Master.Contract
{
    public class Company : IContract
    {
        public Company()
        {


        }

        public string CompanyCode { get; set; }

        public string CompanyName { get; set; }

        public string RegNo { get; set; }


        public object Logo { get; set; }

        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }


        public Contact ContactItem { get; set; }

        private List<Contact> contactItems = new List<Contact>();

        public List<Contact> ContactItems
        {
            get { return contactItems; }
            set { contactItems = value; }
        }


        public List<Branch> BranchList { get; set; }

    }
}
