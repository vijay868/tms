﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;
using TMS.Master.BusinessFactory;
using TMS.Master.DataFactory;

namespace TMS.ServiceContainer
{
    public class MasterModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterType<CountryBO>().SingleInstance();
            builder.RegisterType<LookupBO>().SingleInstance();
            builder.RegisterType<ContactBO>().SingleInstance();
            builder.RegisterType<CustomerBO>().SingleInstance();
            builder.RegisterType<PortBO>().SingleInstance();

            builder.RegisterType<CompanyBO>().SingleInstance();
            builder.RegisterType<BranchBO>().SingleInstance();

            builder.RegisterType<EquipmentTypeSizeBO>().SingleInstance();
            builder.RegisterType<ContainerStatusBO>().SingleInstance();
            builder.RegisterType<ChargeCodeBO>().SingleInstance();
            builder.RegisterType<VesselBO>().SingleInstance();


            builder.RegisterType<CountryDAL>().SingleInstance();
            builder.RegisterType<LookupDAL>().SingleInstance();
            builder.RegisterType<ContactDAL>().SingleInstance();
            builder.RegisterType<CustomerDAL>().SingleInstance();
            builder.RegisterType<PortDAL>().SingleInstance();

            builder.RegisterType<CompanyDAL>().SingleInstance();
            builder.RegisterType<BranchDAL>().SingleInstance();


            builder.RegisterType<EquipmentTypeSizeDAL>().SingleInstance();
            builder.RegisterType<ContainerStatusDAL>().SingleInstance();
            builder.RegisterType<ChargeCodeDAL>().SingleInstance();
            builder.RegisterType<VesselDAL>().SingleInstance();
        }
    }
}
