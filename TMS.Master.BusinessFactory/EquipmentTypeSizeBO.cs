﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Master.Contract;
using TMS.Master.DataFactory;

namespace TMS.Master.BusinessFactory
{
    public class EquipmentTypeSizeBO
    {
        private EquipmentTypeSizeDAL equipmenttypesizeDAL;
        public EquipmentTypeSizeBO(EquipmentTypeSizeDAL _equipmentTypeSizeDAL)
        {
            equipmenttypesizeDAL = _equipmentTypeSizeDAL;
        }

        public List<EquipmentTypeSizeMaster> GetEquipmentTypeSizeList()
        {
            return equipmenttypesizeDAL.GetList();
        }

        public bool SaveEquipmentTypeSize(EquipmentTypeSizeMaster newItem)
        {

            return equipmenttypesizeDAL.Save(newItem);

        }

        public bool DeleteEquipmentTypeSize(EquipmentTypeSizeMaster item)
        {
            return equipmenttypesizeDAL.Delete(item);
        }

        public EquipmentTypeSizeMaster GetEquipmentTypeSize(EquipmentTypeSizeMaster item)
        {
            return (EquipmentTypeSizeMaster)equipmenttypesizeDAL.GetItem<EquipmentTypeSizeMaster>(item);
        }
    }
}
