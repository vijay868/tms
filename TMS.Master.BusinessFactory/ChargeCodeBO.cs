﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using TMS.Master.Contract;
using TMS.Master.DataFactory;

namespace TMS.Master.BusinessFactory
{
    public class ChargeCodeBO
    {
        private ChargeCodeDAL chargecodeDAL;
        public ChargeCodeBO(ChargeCodeDAL _chargecodeDAL)
        {

            chargecodeDAL = _chargecodeDAL;
        }

        public List<ChargeCodeMaster> GetChargeCodeList()
        {
            return chargecodeDAL.GetList();
        }


        public bool SaveChargeCode(ChargeCodeMaster newItem)
        {

            return chargecodeDAL.Save(newItem);

        }

        public bool DeleteChargeCode(ChargeCodeMaster item)
        {
            return chargecodeDAL.Delete(item);
        }

        public ChargeCodeMaster GetChargeCode(ChargeCodeMaster item)
        {
            return (ChargeCodeMaster)chargecodeDAL.GetItem<ChargeCodeMaster>(item);
        }

        public IDataReader PerformSearch(string whereClause)
        {
            return chargecodeDAL.PerformSearch(whereClause);
        }
    }
}
