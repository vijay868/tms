﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMS.Master.Contract;
using TMS.Master.DataFactory;

namespace TMS.Master.BusinessFactory
{
    public class BranchBO
    {
        private BranchDAL branchDAL;
        public BranchBO(BranchDAL _branchDAL)
        {

            branchDAL = _branchDAL;
        }

        public List<Branch> GetBranchList(string companyCode)
        {
            return branchDAL.GetListByCompanyCode(companyCode);
        }

        public List<Branch> GetBranchList()
        {
            return branchDAL.GetList();
        }


        public bool SaveBranch(Branch newItem)
        {

            if (newItem.BranchID == 0)
            {
                var IsBranchExist = branchDAL.GetList()
                                             .Where(b => b.BranchCode == newItem.BranchCode)
                                             .SingleOrDefault();

                if (IsBranchExist != null)
                    throw new Exception(string.Format("Branch Code < {0} > already Exists Under Company {1}.\r\n the current status is {2}",
                                                      newItem.BranchCode, IsBranchExist.CompanyCode, IsBranchExist.IsActive == true ? "Active" : "InActive"));
            }

            return branchDAL.Save(newItem);

        }

        public bool DeleteBranch(Branch item)
        {
            return branchDAL.Delete(item);
        }

        public Branch GetBranch(Branch item)
        {
            return (Branch)branchDAL.GetItem<Branch>(item);
        }

    }
}
