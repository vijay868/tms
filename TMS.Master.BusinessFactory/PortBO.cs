﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS.Master.Contract;
using TMS.Master.Contract.Dto;
using TMS.Master.DataFactory;

namespace TMS.Master.BusinessFactory
{
    public class PortBO
    {
        private PortDAL portDAL;
        private LookupBO lookupBO;
        public PortBO(PortDAL _contactDAL, LookupBO _lookupBO)
        {
            portDAL = _contactDAL;
            lookupBO = _lookupBO;
        }

        public List<Port> GetPortList()
        {
            return portDAL.GetList();
        }


        public List<Port> GetWharfList()
        {
            return portDAL.GetWharfList();
        }


        public string GetPaperLessCode(string wharfcode)
        {

            var wharfCode = lookupBO.GetLookupByCategory("AreaPortType")
                                          .Where(apt => apt.LookupCode == "WHARF")
                                          .SingleOrDefault().LookupID;



            var result = portDAL.GetWharfList()
                                .Where(w => w.PortAreaCode == wharfcode && w.AreaPortType == wharfCode)
                                .FirstOrDefault();

            if (result == null) return "";

            return result.PaperLessCode.ToString();
        }

        public bool SavePort(Port newItem)
        {

            return portDAL.Save(newItem);

        }

        public bool DeletePort(Port item)
        {
            return portDAL.Delete(item);
        }

        public Port GetPort(Port item)
        {
            return (Port)portDAL.GetItem<Port>(item);
        }

        public List<Port> GetLocationList()
        {
            return portDAL.GetLocationList();
        }

        public IDataReader PerformSearch(string whereClause)
        {
            return portDAL.PerformSearch(whereClause);
        }

        public List<PortDto> PerformSearchWrapper(string portName)
        {
            var like = $"PortAreaCode like '%{portName}%'";
            var rdr = PerformSearch(like);

            List<PortDto> portDtoList = new List<PortDto>();
            PortDto portDto;
            while (rdr.Read())
            {
                portDto = new PortDto();
                portDto.PortAreaCode = rdr["PortAreaCode"].ToString();
                portDto.PortAreaName = rdr["PortAreaName"].ToString();

                portDtoList.Add(portDto);
            }

            return portDtoList.Take(20).ToList();
        }

    }
}
