﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMS.Master.Contract;
using TMS.Master.Contract.Dto;
using TMS.Master.DataFactory;

namespace TMS.Master.BusinessFactory
{
    public class LookupBO
    {
        private LookupDAL lookupDAL;
        private EquipmentTypeSizeDAL equipmentTypeSizeDAL;
        private ContainerStatusDAL containerStatusDAL;
        public LookupBO(LookupDAL _lookupDAL, EquipmentTypeSizeDAL _equipmentTypeSizeDAL, ContainerStatusDAL _containerStatusDAL)
        {
            lookupDAL = _lookupDAL;
            equipmentTypeSizeDAL = _equipmentTypeSizeDAL;
            containerStatusDAL = _containerStatusDAL;
        }

        private List<LookUpDto> GetLookupByCategory(List<Lookup> data, string category)
        {
            return data.Where(x => x.LookupCategory == category)
                                .Select(x => new LookUpDto
                                {
                                    LookupID = x.LookupID,
                                    LookupCategory = x.LookupCategory,
                                    LookupCode = x.LookupCode,
                                    LookupDescription = x.LookupDescription
                                }).ToList();
        }

        public object GetLookupList()
        {
            // return lookupDataService.GetLookupList();

            var data = lookupDAL.GetLookupList();

            var AreaPortType = GetLookupByCategory(data, "AreaPortType");

            var BillingMode = GetLookupByCategory(data, "BillingMode");

            var BookingType = GetLookupByCategory(data, "BookingType");

            var CargoCategory = GetLookupByCategory(data, "CargoCategory");

            var CargoType = GetLookupByCategory(data, "CargoType");

            var ChargeTerm = GetLookupByCategory(data, "ChargeTerm");

            var ChargeType = GetLookupByCategory(data, "ChargeType");

            var ChargeTypeTrucking = GetLookupByCategory(data, "ChargeType-Trucking");

            var ChasisSize = GetLookupByCategory(data, "ChasisSize");

            var ChasisStatus = GetLookupByCategory(data, "ChasisStatus");

            var ChasisType = GetLookupByCategory(data, "ChasisType");
            var ComponentGroup = GetLookupByCategory(data, "ComponentGroup");
            var ContainerCondition = GetLookupByCategory(data, "ContainerCondition");
            var ContainerGrade = GetLookupByCategory(data, "ContainerGrade");

            var ContainerHeight = GetLookupByCategory(data, "ContainerHeight");

            var ContainerHireMode = GetLookupByCategory(data, "ContainerHireMode");

            var ContainerMaterial = GetLookupByCategory(data, "ContainerMaterial");

            var ContainerMode = GetLookupByCategory(data, "ContainerMode");

            var ContainerReeferUnit = GetLookupByCategory(data, "ContainerReeferUnit");

            var Currency = GetLookupByCategory(data, "CURRENCY");

            var DamageCode = GetLookupByCategory(data, "DamageCode");

            var DamageIndicator = GetLookupByCategory(data, "DamageIndicator");
            var DiscountType = GetLookupByCategory(data, "DiscountType");

            var DriverStatus = GetLookupByCategory(data, "DriverStatus");
            var DropOffMode = GetLookupByCategory(data, "DropOffMode");
            var EfIndicator = GetLookupByCategory(data, "E/FIndicator");
            var EmrwareHouse = GetLookupByCategory(data, "EMRWAREHOUSE");
            var EorType = GetLookupByCategory(data, "EORType");
            var FuelTankID = GetLookupByCategory(data, "FuelTankID");
            var FuelUom = GetLookupByCategory(data, "FuelUOM");
            var ImageType = GetLookupByCategory(data, "ImageType");
            var Imo = GetLookupByCategory(data, "IMO");
            var InvoiceType = GetLookupByCategory(data, "InvoiceType");
            var Module = GetLookupByCategory(data, "Module");
            var MovementIndicator = GetLookupByCategory(data, "MovementIndicator");
            var MovementStatus = GetLookupByCategory(data, "MovementStatus");
            var OrderType = GetLookupByCategory(data, "OrderType");
            var OwnerApprovalStatus = GetLookupByCategory(data, "OwnerApprovalStatus");
            var Ownership = GetLookupByCategory(data, "Ownership");
            var PaymentTerm = GetLookupByCategory(data, "PaymentTerm");
            var PaymentTo = GetLookupByCategory(data, "PaymentTo");
            var PickupMode = GetLookupByCategory(data, "PickupMode");
            var RepairGroup = GetLookupByCategory(data, "RepairGroup");
            var RepairMode = GetLookupByCategory(data, "RepairMode");
            var RepoMode = GetLookupByCategory(data, "RepoMode");
            var Responsibility = GetLookupByCategory(data, "RESPONSIBILITY");
            var ServiceType = GetLookupByCategory(data, "ServiceType");
            var ShipmentType = GetLookupByCategory(data, "ShipmentType");
            var StockLedger = GetLookupByCategory(data, "StockLedger");
            var TankInventoryType = GetLookupByCategory(data, "TankInventoryType");
            var TempratureMode = GetLookupByCategory(data, "TempratureMode");

            var TradeMode = GetLookupByCategory(data, "TradeMode");

            var TransportMode = GetLookupByCategory(data, "TransportMode");
            var TripType = GetLookupByCategory(data, "TripType");
            var TrnBillingUnit = GetLookupByCategory(data, "TrnBillingUnit");
            var TruckActivityIndicator = GetLookupByCategory(data, "TruckActivityIndicator");
            var TruckCategory = GetLookupByCategory(data, "TruckCategory");
            var TruckMake = GetLookupByCategory(data, "TruckMake");
            var TruckStatus = GetLookupByCategory(data, "TruckStatus");
            var Uom = GetLookupByCategory(data, "UOM");
            var VentilationMode = GetLookupByCategory(data, "VentilationMode");
            var WithHoldingTax = GetLookupByCategory(data, "WithHoldingTax");
            var WoApprovalStatus = GetLookupByCategory(data, "WOApprovalStatus");
            var YardIndicator = GetLookupByCategory(data, "YardIndicator");
            var YardType = GetLookupByCategory(data, "YardType");

            var EquipmentSize = equipmentTypeSizeDAL.GetList()
                .Select(x => new LookUpDto
                {
                    LookupID =null,
                    LookupCode = x.EquipmentSize,
                    LookupDescription = x.EquipmentSize,
                    LookupCategory = "Size"
                }).Distinct().ToList();
            var EquipmentType = equipmentTypeSizeDAL.GetList()
                .Select(x => new LookUpDto
                {
                    LookupID = null,
                    LookupCode = x.EquipmentType,
                    LookupDescription = x.EquipmentType,
                    LookupCategory = "Type"
                }).Distinct().ToList();
            var ContainerStatus = containerStatusDAL.GetList()
                .Where(st => st.Status == true)
                .Select(x => new LookUpDto
                {
                    LookupID = null,
                    LookupCode = x.Code,
                    LookupDescription = x.Description,
                    LookupCategory = "ContainerStatus"
                }).Distinct().ToList();

            return new
            {
                AreaPortType,
                BillingMode,
                BookingType,
                CargoCategory,
                CargoType,
                ChargeTerm,
                ChargeType,
                ChargeTypeTrucking,
                ChasisSize,
                ChasisStatus,
                ChasisType,
                ComponentGroup,
                ContainerCondition,
                ContainerGrade,
                ContainerHeight,
                ContainerHireMode,
                ContainerMaterial,
                ContainerMode,
                ContainerReeferUnit,
                Currency,
                DamageCode,
                DamageIndicator,
                DiscountType,
                DriverStatus,
                DropOffMode,
                EfIndicator,
                EmrwareHouse,
                EorType,
                FuelTankID,
                FuelUom,
                ImageType,
                Imo,
                InvoiceType,
                Module,
                MovementIndicator,
                MovementStatus,
                OrderType,
                OwnerApprovalStatus,
                Ownership,
                PaymentTerm,
                PaymentTo,
                PickupMode,
                RepairGroup,
                RepairMode,
                RepoMode,
                Responsibility,
                ServiceType,
                ShipmentType,
                StockLedger,
                TankInventoryType,
                TempratureMode,
                TradeMode,
                TransportMode,
                TripType,
                TrnBillingUnit,
                TruckActivityIndicator,
                TruckCategory,
                TruckMake,
                TruckStatus,
                Uom,
                VentilationMode,
                WithHoldingTax,
                WoApprovalStatus,
                YardIndicator,
                YardType,
                EquipmentSize,
                EquipmentType,
                ContainerStatus
            };
        }

        public List<Lookup> GetLookupByCategory(string category = "")
        {
            return lookupDAL.GetListByCategory(category);
        }


        public bool SaveLookup(Lookup newItem)
        {

            return lookupDAL.Save(newItem);

        }

        public bool DeleteLookup(Lookup item)
        {
            return lookupDAL.Delete(item);
        }

        public Lookup GetLookup(Lookup item)
        {
            return (Lookup)lookupDAL.GetItem<Lookup>(item);
        }

        public List<Lookup> GetLookupCategories()
        {
            return lookupDAL.GetLookupCategories();
        }

    }
}
