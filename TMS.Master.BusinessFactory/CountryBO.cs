﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Common;
using TMS.Master.Contract;

using TMS.Master.DataFactory;

namespace TMS.Master.BusinessFactory
{
    public class CountryBO
    {
        private CountryDAL countryDAL;
        public CountryBO(CountryDAL _countryDAL)
        {
            countryDAL = _countryDAL;
        }

        public IEnumerable<Country> GetList()
        {
            return countryDAL.GetList();
        }


        public bool Save(Country newItem)
        {
            return countryDAL.Save(newItem);
        }

        public bool Delete(Country item)
        {
            return countryDAL.Delete(item);
        }

        public Country Get(Country item)
        {
            return (Country)countryDAL.GetItem<Country>(item);
        }
    }
}
