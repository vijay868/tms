﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Master.Contract;
using TMS.Master.DataFactory;

namespace TMS.Master.BusinessFactory
{
    public class ContactBO
    {
        private ContactDAL contactDAL;
        public ContactBO(ContactDAL _contactDAL)
        {
            contactDAL = _contactDAL;
        }

        public List<Contact> GetContactList()
        {
            return contactDAL.GetList();
        }


        public bool SaveContact(Contact newItem)
        {

            return contactDAL.Save(newItem);

        }

        public bool DeleteContact(Contact item)
        {
            return contactDAL.Delete(item);
        }

        public Contact GetContact(Contact item)
        {
            return (Contact)contactDAL.GetItem<Contact>(item);
        }
    }
}
