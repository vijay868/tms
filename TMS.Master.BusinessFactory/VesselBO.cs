﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using TMS.Master.Contract;
using TMS.Master.DataFactory;

namespace TMS.Master.BusinessFactory
{
    public class VesselBO
    {
        private VesselDAL vesselDAL;
        public VesselBO(VesselDAL _vesselDAL)
        {
            vesselDAL = _vesselDAL;
        }

        public List<Vessel> GetVesselList()
        {
            return vesselDAL.GetList();
        }

        public bool SaveVessel(Vessel newItem)
        {
            return vesselDAL.Save(newItem);
        }

        public bool DeleteVessel(Vessel item)
        {
            return vesselDAL.Delete(item);
        }

        public Vessel GetVessel(Vessel item)
        {
            return (Vessel)vesselDAL.GetItem<Vessel>(item);
        }

        public IDataReader PerformSearch(string whereClause)
        {
            return vesselDAL.PerformSearch(whereClause);
        }

    }
}
