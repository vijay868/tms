﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Master.Contract;
using TMS.Master.DataFactory;

namespace TMS.Master.BusinessFactory
{
    public class ContainerStatusBO
    {
        private ContainerStatusDAL containerstatusDAL;
        public ContainerStatusBO(ContainerStatusDAL _containerstatusDAL)
        {
            containerstatusDAL = _containerstatusDAL;
        }
        public List<ContainerStatus> GetList()
        {
            return containerstatusDAL.GetList();
        }


        public bool SaveContainerStatus(ContainerStatus newItem)
        {

            return containerstatusDAL.Save(newItem);

        }

        public bool DeleteContainerStatus(ContainerStatus item)
        {
            return containerstatusDAL.Delete(item);
        }

        public ContainerStatus GetContainerStatus(ContainerStatus item)
        {
            return (ContainerStatus)containerstatusDAL.GetItem<ContainerStatus>(item);
        }

    }
}
