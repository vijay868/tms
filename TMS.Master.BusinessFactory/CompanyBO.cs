﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Master.Contract;
using TMS.Master.DataFactory;

namespace TMS.Master.BusinessFactory
{
    public class CompanyBO
    {
        private CompanyDAL companyDAL;
        public CompanyBO(CompanyDAL _companyDAL)
        {
            companyDAL = _companyDAL;
        }

        public List<Company> GetCompanyList()
        {
            return companyDAL.GetList();
        }


        public bool SaveCompany(Company newItem)
        {

            return companyDAL.Save(newItem);

        }

        public bool DeleteCompany(Company item)
        {
            return companyDAL.Delete(item);
        }

        public Company GetCompany(Company item)
        {
            return (Company)companyDAL.GetItem<Company>(item);
        }

    }
}
