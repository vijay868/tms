﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TMS.Master.Contract;
using TMS.Master.Contract.Dto;
using TMS.Master.DataFactory;

namespace TMS.Master.BusinessFactory
{
    public class CustomerBO
    {
        private CustomerDAL customerDAL;
        public CustomerBO(CustomerDAL _customerDAL)
        {
            customerDAL = _customerDAL;
        }

        public List<Customer> GetCustomerList()
        {
            return customerDAL.GetList();
        }

        public List<Customer> GetAgentList()
        {
            return customerDAL.GetAgentList();
        }

        public bool SaveCustomer(Customer newItem)
        {
            return customerDAL.Save(newItem);
        }

        public bool DeleteCustomer(Customer item)
        {
            return customerDAL.Delete(item);
        }

        public Customer GetCustomer(Customer item)
        {
            return (Customer)customerDAL.GetItem<Customer>(item);
        }

        public IDataReader PerformSearch(string customertypefiler, string whereClause)
        {
            return customerDAL.PerformSearch(customertypefiler, whereClause);
        }

        public List<SearchDto> PerformSearchWrapper(string customertypefiler, string whereClause)
        {
            var like = $"CustomerName like '%{whereClause}%'"; 
            var rdr = PerformSearch(customertypefiler, like);

            List<SearchDto> searchDtoList = new List<SearchDto>();
            SearchDto searchDto;
            while(rdr.Read())
            {
                searchDto = new SearchDto();
                searchDto.CustomerCode = rdr[0].ToString();
                searchDto.CustomerName = rdr[1].ToString();

                searchDtoList.Add(searchDto);
            }

            return searchDtoList.Take(20).ToList();
        }

        public Customer GetCustomerByName(Customer item)
        {
            return customerDAL.GetCustomerByName(item);
        }
    }
}
