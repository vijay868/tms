﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TMS.Common.Enums
{
    public enum CustomerCode
    {
        AgentCode,
        ForwarderCode,
        CustomerCode,
        HaulierCode,
        TransporterCode
    }
}
