﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Data.Common;

using Microsoft.Extensions.Options;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using TMS.Common;
using TMS.Master.Contract;
using System.Linq;
using TMS.Common.Enums;

namespace TMS.Master.DataFactory
{
    public class CustomerDAL
    {
        private Database db;
        private ContactDAL contactDAL;
        /// <summary>
        /// Constructor
        /// </summary>
        public CustomerDAL(
            IOptions<DataBaseConfiguration> config,
            ContactDAL _contactDAL)
        {
            db = new SqlDatabase(config.Value.dbString);
            contactDAL = _contactDAL;

        }

        #region IDataFactory Members

        public List<Customer> GetList()
        {
            return db.ExecuteSprocAccessor(DBRoutine.LISTCUSTOMER, MapBuilder<Customer>.BuildAllProperties()).ToList();
        }

        public List<Customer> GetAgentList()
        {
            return db.ExecuteSprocAccessor(DBRoutine.LISTAGENT, MapBuilder<Customer>.BuildAllProperties()).ToList();
        }


        public Customer GetCustomerByName<T>(T item) where T : IContract
        {

            var customer = (Customer)(object)item;

            var customerItem = db.ExecuteSprocAccessor(DBRoutine.SELECTCUSTOMERBYNAME,
                                                        MapBuilder<Customer>.BuildAllProperties(),
                                                        customer.CustomerName).SingleOrDefault();


            if (customerItem == null)
            {
                return null;
            }
            var contactItem = new Master.Contract.Contact
            {
                AddressLinkID = customerItem.CustomerCode,
                EntityType = "Customer"
            };

            customerItem.ContactItems = contactDAL.GetContactsByCustomer(contactItem);

            return customerItem;
        }

        public bool Save<T>(T item) where T : IContract
        {
            var result = 0;

            var customer = (Customer)(object)item;

            var connection = db.CreateConnection();
            connection.Open();


            var transaction = connection.BeginTransaction();
            try
            {
                var savecommand = db.GetStoredProcCommand(DBRoutine.SAVECUSTOMER);

                db.AddInParameter(savecommand, "CustomerCode", System.Data.DbType.String, customer.CustomerCode);
                db.AddInParameter(savecommand, "CustomerName", System.Data.DbType.String, customer.CustomerName);
                db.AddInParameter(savecommand, "CustomerName2", System.Data.DbType.String, customer.CustomerName2);
                db.AddInParameter(savecommand, "RegistrationNo", System.Data.DbType.String, customer.RegistrationNo);
                db.AddInParameter(savecommand, "IsBillingCustomer", System.Data.DbType.Boolean, customer.IsBillingCustomer);
                db.AddInParameter(savecommand, "IsShipper", System.Data.DbType.Boolean, customer.IsShipper);
                db.AddInParameter(savecommand, "IsConsignee", System.Data.DbType.Boolean, customer.IsConsignee);
                db.AddInParameter(savecommand, "IsFwdAgent", System.Data.DbType.Boolean, customer.IsFwdAgent);
                db.AddInParameter(savecommand, "IsShippingAgent", System.Data.DbType.Boolean, customer.IsShippingAgent);
                db.AddInParameter(savecommand, "IsHaulier", System.Data.DbType.Boolean, customer.IsHaulier);
                db.AddInParameter(savecommand, "IsTransporter", System.Data.DbType.Boolean, customer.IsTransporter);
                db.AddInParameter(savecommand, "OperatorCode", System.Data.DbType.String, customer.OperatorCode);
                db.AddInParameter(savecommand, "CurrencyCode", System.Data.DbType.String, customer.CurrencyCode);
                db.AddInParameter(savecommand, "DebtorCode", System.Data.DbType.String, customer.DebtorCode);
                db.AddInParameter(savecommand, "RevenueAccount", System.Data.DbType.String, customer.RevenueAccount);
                db.AddInParameter(savecommand, "Status", System.Data.DbType.Boolean, customer.Status);
                db.AddInParameter(savecommand, "Remark", System.Data.DbType.String, customer.Remark);
                db.AddInParameter(savecommand, "NumberOfLongStandingDays", System.Data.DbType.Int16, customer.NumberOfLongStandingDays);
                db.AddInParameter(savecommand, "CreditTerm", System.Data.DbType.Int16, customer.CreditTerm);
                db.AddInParameter(savecommand, "CreatedBy", System.Data.DbType.String, customer.CreatedBy);
                db.AddInParameter(savecommand, "ModifiedBy", System.Data.DbType.String, customer.ModifiedBy);
                db.AddInParameter(savecommand, "IsNewRecord", System.Data.DbType.Boolean, customer.IsNewRecord);
                //db.AddInParameter(savecommand, "AuditID", System.Data.DbType.String, customer.AuditID);


                result = db.ExecuteNonQuery(savecommand, transaction);

                if (result > 0)
                {
                    result = contactDAL.SaveList(customer.ContactItems, transaction) == true ? 1 : 0;
                }

                if (result > 0)
                    transaction.Commit();
                else
                    transaction.Rollback();



            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }


            return (result > 0 ? true : false);

        }

        public bool Delete<T>(T item) where T : IContract
        {
            var result = false;
            var customer = (Customer)(object)item;

            using (DbConnection connnection = db.CreateConnection())
            {
                connnection.Open();
                using (DbTransaction transaction = connnection.BeginTransaction())
                {
                    try
                    {
                        var deleteCommand = db.GetStoredProcCommand(DBRoutine.DELETECUSTOMER);

                        db.AddInParameter(deleteCommand, "CustomerCode", System.Data.DbType.String, customer.CustomerCode);
                        db.AddInParameter(deleteCommand, "ModifiedBy", System.Data.DbType.String, customer.ModifiedBy);

                        result = Convert.ToBoolean(db.ExecuteNonQuery(deleteCommand, transaction));

                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }

                }
            }
            return result;
        }

        public IContract GetItem<T>(IContract lookupItem) where T : IContract
        {

            //IRowMapper<Customer> rowmapper = MapBuilder<Customer>.BuildAllProperties();
            //IParameterMapper parametermapper = new SelectCustomerParameter();

            //var accessor = db.CreateSprocAccessor(DBRoutine.SELECTCUSTOMER, parametermapper, rowmapper);
            //var customerItem = accessor.Execute(((Customer)lookupItem).CustomerCode).FirstOrDefault();


            var customerItem = db.ExecuteSprocAccessor(DBRoutine.SELECTCUSTOMER,
                                                       MapBuilder<Customer>
                                                            .MapAllProperties()
                                                            .DoNotMap(c => c.ContactItems)
                                                            .Build(),
                                                            ((Customer)lookupItem).CustomerCode
                                                            ).FirstOrDefault();



            if (customerItem == null)
            {
                return null;
            }
            var contactItem = new Master.Contract.Contact
            {
                AddressLinkID = customerItem.CustomerCode,
                EntityType = "Customer"
            };

            customerItem.ContactItems = contactDAL.GetContactsByCustomer(contactItem);


            return customerItem;
        }


        public System.Data.IDataReader PerformSearch(string customertypefiler, string whereClause)
        {

            var searchcommand = db.GetStoredProcCommand(DBRoutine.SEARCHCUSTOMER);

            db.AddInParameter(searchcommand, "CustomerType", System.Data.DbType.String, customertypefiler);
            db.AddInParameter(searchcommand, "whereClause", System.Data.DbType.String, whereClause);

            //var tempReader = (SqlDataReader)db.ExecuteReader(searchcommand);
            //return (SqlDataReader)tempReader.InnerReader;

            return db.ExecuteReader(searchcommand);


        }        

        #endregion


        /// <summary>
        /// private classes for assigning parameters to the Accessor
        /// </summary>
        internal class SelectCustomerParameter : IParameterMapper
        {


            #region IParameterMapper Members

            public void AssignParameters(DbCommand command, object[] parameterValues)
            {
                var parameter = command.CreateParameter();
                parameter.ParameterName = "CustomerCode";
                parameter.Value = parameterValues[0];
                command.Parameters.Add(parameter);
            }

            #endregion
        }
    }
}
