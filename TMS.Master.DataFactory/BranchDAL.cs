﻿using Microsoft.Extensions.Options;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using TMS.Common;
using TMS.Master.Contract;

namespace TMS.Master.DataFactory
{
    public class BranchDAL
    {

        /// <summary>
        /// Constructor
        /// </summary>
        private Database db;
        private DbTransaction currentTransaction = null;
        private DbConnection connection = null;
        /// <summary>
        /// Constructor
        /// </summary>
        private ContactDAL contactDAL;
        public BranchDAL(IOptions<DataBaseConfiguration> config, ContactDAL _contactDAL)
        {
            db = new SqlDatabase(config.Value.dbString);
            contactDAL = _contactDAL;

        }

        #region IDataFactory Members

        public List<Branch> GetList()
        {
            return db.ExecuteSprocAccessor(DBRoutine.LISTBRANCH, MapBuilder<Branch>
                                                            .MapAllProperties()
                                                            .DoNotMap(c => c.ContactItem)
                                                            .Build()).ToList();
        }

        public List<Branch> GetListByCompanyCode(string companycode)
        {

            return db.ExecuteSprocAccessor(DBRoutine.LISTBRANCHBYCOMPANY, MapBuilder<Branch>
                                                            .MapAllProperties()
                                                            .DoNotMap(c => c.ContactItem)
                                                            .Build(), companycode).ToList();
        }





        public bool Save<T>(T item) where T : IContract
        {
            var result = 0;

            var branch = (Branch)(object)item;

            var connection = db.CreateConnection();

            connection.Open();


            var transaction = connection.BeginTransaction();
            try
            {
                var savecommand = db.GetStoredProcCommand(DBRoutine.SAVEBRANCH);

                db.AddInParameter(savecommand, "BranchID", System.Data.DbType.Int16, branch.BranchID);
                db.AddInParameter(savecommand, "BranchCode", System.Data.DbType.String, branch.BranchCode);
                db.AddInParameter(savecommand, "BranchName", System.Data.DbType.String, branch.BranchName);
                db.AddInParameter(savecommand, "RegNo", System.Data.DbType.String, branch.RegNo);
                db.AddInParameter(savecommand, "CompanyCode", System.Data.DbType.String, branch.CompanyCode);
                db.AddInParameter(savecommand, "CreatedBy", System.Data.DbType.String, branch.CreatedBy);
                db.AddInParameter(savecommand, "ModifiedBy", System.Data.DbType.String, branch.ModifiedBy);



                result = db.ExecuteNonQuery(savecommand, transaction);

                if (result > 0)
                {                    
                    result = contactDAL.Save(branch.ContactItem, transaction) == true ? 1 : 0;
                }

                if (result > 0)
                    transaction.Commit();
                else
                    transaction.Rollback();

            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }



            return (result > 0 ? true : false);

        }

        public bool Delete<T>(T item) where T : IContract
        {
            var result = false;
            var branch = (Branch)(object)item;

            using (DbConnection connnection = db.CreateConnection())
            {
                connnection.Open();
                using (DbTransaction transaction = connnection.BeginTransaction())
                {
                    try
                    {
                        var deleteCommand = db.GetStoredProcCommand(DBRoutine.DELETEBRANCH);

                        db.AddInParameter(deleteCommand, "BranchID", System.Data.DbType.Int16, branch.BranchID);

                        result = Convert.ToBoolean(db.ExecuteNonQuery(deleteCommand, transaction));

                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }

                }
            }
            return result;
        }

        public IContract GetItem<T>(IContract lookupItem) where T : IContract
        {

            IRowMapper<Branch> rowmapper = MapBuilder<Branch>
                                                        .MapAllProperties()
                                                        .DoNotMap(c => c.ContactItem)
                                                        .Build();

            IParameterMapper parametermapper = new SelectBranchParameter("BranchID");

            var accessor = db.CreateSprocAccessor(DBRoutine.SELECTBRANCH, parametermapper, rowmapper);
            var branchItem = accessor.Execute(((Branch)lookupItem).BranchID).FirstOrDefault();


            var contactItem =

            branchItem.ContactItems = contactDAL.GetContactsByCustomer(new Master.Contract.Contact
            {
                AddressLinkID = branchItem.BranchCode,
                EntityType = "Branch"
            });
            return branchItem;
        }

        #endregion


        /// <summary>
        /// private classes for assigning parameters to the Accessor
        /// </summary>
        internal class SelectBranchParameter : IParameterMapper
        {
            private string paramname;

            internal SelectBranchParameter(string paramName)
            {
                paramname = paramName;
            }

            #region IParameterMapper Members

            public void AssignParameters(DbCommand command, object[] parameterValues)
            {
                var parameter = command.CreateParameter();
                parameter.ParameterName = paramname;
                parameter.Value = parameterValues[0];
                command.Parameters.Add(parameter);
            }

            #endregion
        }
    }
}
