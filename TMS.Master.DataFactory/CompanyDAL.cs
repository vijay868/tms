﻿using Microsoft.Extensions.Options;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using TMS.Common;
using TMS.Master.Contract;

namespace TMS.Master.DataFactory
{
    public class CompanyDAL
    {
        private Database db;
        private DbTransaction currentTransaction = null;
        private DbConnection connection = null;
        /// <summary>
        /// Constructor
        /// </summary>
        private ContactDAL contactDAL;
        private BranchDAL branchDAL;
        public CompanyDAL(IOptions<DataBaseConfiguration> config, ContactDAL _contactDAL, BranchDAL _branchDAL)
        {
            db = new SqlDatabase(config.Value.dbString);
            contactDAL = _contactDAL;
            branchDAL = _branchDAL;

        }

        #region IDataFactory Members

        public List<Company> GetList()
        {

            try
            {


                var lstCompany = db.ExecuteSprocAccessor(DBRoutine.LISTCOMPANY, MapBuilder<Company>
                                                                .MapAllProperties()
                                                                .DoNotMap(c => c.ContactItem)
                                                                .Build()).ToList();


                foreach (var companyitem in lstCompany)
                {
                    companyitem.BranchList = branchDAL.GetListByCompanyCode(companyitem.CompanyCode);

                    companyitem.ContactItems = contactDAL.GetContactsByCustomer(new Contact { AddressLinkID = companyitem.CompanyCode, EntityType = "Company" });

                    if (companyitem.BranchList.Count > 0)
                    {
                        foreach (var branchItem in companyitem.BranchList)
                        {
                            branchItem.ContactItems = contactDAL.GetContactsByCustomer(new Contact { AddressLinkID = branchItem.BranchCode, EntityType = "Branch" });
                        }
                    }
                }



                return lstCompany;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool Save<T>(T item) where T : IContract
        {
            var result = 0;

            var company = (Company)(object)item;

            var connection = db.CreateConnection();

            connection.Open();


            var transaction = connection.BeginTransaction();


            try
            {
                var savecommand = db.GetStoredProcCommand(DBRoutine.SAVECOMPANY);

                db.AddInParameter(savecommand, "CompanyCode", System.Data.DbType.String, company.CompanyCode);
                db.AddInParameter(savecommand, "CompanyName", System.Data.DbType.String, company.CompanyName);
                db.AddInParameter(savecommand, "RegNo", System.Data.DbType.String, company.RegNo);
                //db.AddInParameter(savecommand, "Logo", System.Data.DbType.Object, null);
                db.AddInParameter(savecommand, "IsActive", System.Data.DbType.Boolean, company.IsActive);
                db.AddInParameter(savecommand, "CreatedBy", System.Data.DbType.String, company.CreatedBy);
                db.AddInParameter(savecommand, "ModifiedBy", System.Data.DbType.String, company.ModifiedBy);


                result = db.ExecuteNonQuery(savecommand, transaction);
                if (result > 0)
                {
                    
                    result = contactDAL.Save(company.ContactItem, transaction) == true ? 1 : 0;
                }

                if (result > 0)
                    transaction.Commit();
                else
                    transaction.Rollback();

            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }





            return (result > 0 ? true : false);

        }

        public bool Delete<T>(T item) where T : IContract
        {
            var result = false;
            var company = (Company)(object)item;

            using (DbConnection connnection = db.CreateConnection())
            {
                connnection.Open();
                using (DbTransaction transaction = connnection.BeginTransaction())
                {
                    try
                    {
                        var deleteCommand = db.GetStoredProcCommand(DBRoutine.DELETECOMPANY);

                        db.AddInParameter(deleteCommand, "CompanyCode", System.Data.DbType.String, company.CompanyCode);

                        result = Convert.ToBoolean(db.ExecuteNonQuery(deleteCommand, transaction));

                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }

                }
            }
            return result;
        }

        public IContract GetItem<T>(IContract lookupItem) where T : IContract
        {

            IRowMapper<Company> rowmapper = MapBuilder<Company>
                                                .MapAllProperties()
                                                .DoNotMap(c => c.ContactItem)
                                                .Build();
            IParameterMapper parametermapper = new SelectCompanyParameter();

            var accessor = db.CreateSprocAccessor(DBRoutine.SELECTCOMPANY, parametermapper, rowmapper);
            var companyItem = accessor.Execute(((Company)lookupItem).CompanyCode).FirstOrDefault();

            if (companyItem == null)
            {
                return null;
            }
            var contactItem = new Master.Contract.Contact
            {
                AddressLinkID = companyItem.CompanyCode,
                EntityType = "Company"
            };

            companyItem.ContactItems = contactDAL.GetContactsByCustomer(contactItem);


            //companyItem.ContactItem =  new ContactDAL().GetItem(contactItem);

            companyItem.BranchList = branchDAL.GetListByCompanyCode(companyItem.CompanyCode);

            if (companyItem.BranchList.Count > 0)
            {
                foreach (var branchItem in companyItem.BranchList)
                {
                    branchItem.ContactItems = contactDAL.GetContactsByCustomer(new Contact { AddressLinkID = branchItem.BranchCode, EntityType = "Branch" });
                }
            }



            return companyItem;
        }

        #endregion


        /// <summary>
        /// private classes for assigning parameters to the Accessor
        /// </summary>
        internal class SelectCompanyParameter : IParameterMapper
        {


            #region IParameterMapper Members

            public void AssignParameters(DbCommand command, object[] parameterValues)
            {
                var parameter = command.CreateParameter();
                parameter.ParameterName = "CompanyCode";
                parameter.Value = parameterValues[0];
                command.Parameters.Add(parameter);
            }

            #endregion
        }
    }
}
