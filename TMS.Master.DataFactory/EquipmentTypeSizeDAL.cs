﻿using Microsoft.Extensions.Options;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using TMS.Common;
using TMS.Master.Contract;

namespace TMS.Master.DataFactory
{
    public class EquipmentTypeSizeDAL
    {
        private Database db;

        /// <summary>
        /// Constructor
        /// </summary>
        public EquipmentTypeSizeDAL(IOptions<DataBaseConfiguration> config)
        {
            db = new SqlDatabase(config.Value.dbString);
        }

        #region IDataFactory Members

        public List<EquipmentTypeSizeMaster> GetList()
        {
            return db.ExecuteSprocAccessor(DBRoutine.LISTEQUIPMENTTYPESIZE, MapBuilder<EquipmentTypeSizeMaster>.BuildAllProperties()).ToList();
        }

        public bool Save<T>(T item) where T : IContract
        {
            var result = 0;

            var equipmenttypesizemaster = (EquipmentTypeSizeMaster)(object)item;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        var savecommand = db.GetStoredProcCommand(DBRoutine.SAVEEQUIPMENTTYPESIZE);

                        db.AddInParameter(savecommand, "EquipmentSize", System.Data.DbType.String, equipmenttypesizemaster.EquipmentSize);
                        db.AddInParameter(savecommand, "EquipmentType", System.Data.DbType.String, equipmenttypesizemaster.EquipmentType);
                        db.AddInParameter(savecommand, "Description", System.Data.DbType.String, equipmenttypesizemaster.Description);
                        db.AddInParameter(savecommand, "GrossWeight", System.Data.DbType.Decimal, equipmenttypesizemaster.GrossWeight);
                        db.AddInParameter(savecommand, "Height", System.Data.DbType.Int16, equipmenttypesizemaster.Height);
                        db.AddInParameter(savecommand, "IsContainer", System.Data.DbType.Boolean, equipmenttypesizemaster.IsContainer);
                        db.AddInParameter(savecommand, "ISOCode", System.Data.DbType.String, equipmenttypesizemaster.ISOCode);
                        db.AddInParameter(savecommand, "IsReefer", System.Data.DbType.Boolean, equipmenttypesizemaster.IsReefer);
                        db.AddInParameter(savecommand, "IsTruck", System.Data.DbType.Boolean, equipmenttypesizemaster.IsTruck);
                        db.AddInParameter(savecommand, "MappingCode", System.Data.DbType.String, equipmenttypesizemaster.MappingCode);
                        db.AddInParameter(savecommand, "TareWeight", System.Data.DbType.Decimal, equipmenttypesizemaster.TareWeight);
                        db.AddInParameter(savecommand, "MaxGrossWeight", System.Data.DbType.Decimal, equipmenttypesizemaster.MaxGrossWeight);
                        db.AddInParameter(savecommand, "TEU", System.Data.DbType.Int16, equipmenttypesizemaster.TEU);
                        db.AddInParameter(savecommand, "CreatedBy", System.Data.DbType.String, equipmenttypesizemaster.CreatedBy);
                        db.AddInParameter(savecommand, "ModifiedBy", System.Data.DbType.String, equipmenttypesizemaster.ModifiedBy);
                        db.AddInParameter(savecommand, "Status", System.Data.DbType.Boolean, equipmenttypesizemaster.Status);


                        result = db.ExecuteNonQuery(savecommand, transaction);

                        transaction.Commit();

                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

            }


            return (result > 0 ? true : false);

        }

        public bool Delete<T>(T item) where T : IContract
        {
            var result = false;
            var equipmenttypesizemaster = (EquipmentTypeSizeMaster)(object)item;

            using (DbConnection connnection = db.CreateConnection())
            {
                connnection.Open();
                using (DbTransaction transaction = connnection.BeginTransaction())
                {
                    try
                    {
                        var deleteCommand = db.GetStoredProcCommand(DBRoutine.DELETEEQUIPMENTTYPESIZE);

                        db.AddInParameter(deleteCommand, "EquipmentSize", System.Data.DbType.String, equipmenttypesizemaster.EquipmentSize);
                        db.AddInParameter(deleteCommand, "EquipmentType", System.Data.DbType.String, equipmenttypesizemaster.EquipmentType);

                        result = Convert.ToBoolean(db.ExecuteNonQuery(deleteCommand, transaction));

                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }

                }
            }
            return result;
        }

        public IContract GetItem<T>(IContract lookupItem) where T : IContract
        {

            IRowMapper<EquipmentTypeSizeMaster> rowmapper = MapBuilder<EquipmentTypeSizeMaster>.BuildAllProperties();
            IParameterMapper parametermapper = new SelectEquipmentTypeSizeParameter("EquipmentSize", "EquipmentType");
            
            var accessor = db.CreateSprocAccessor(DBRoutine.SELECTEQUIPMENTTYPESIZE, parametermapper, rowmapper);
            var equipmentTypeSizeItem = accessor.Execute(((EquipmentTypeSizeMaster)lookupItem).EquipmentSize, 
                                                        ((EquipmentTypeSizeMaster)lookupItem).EquipmentType).FirstOrDefault();
            return equipmentTypeSizeItem;
        }

        #endregion


        /// <summary>
        /// private classes for assigning parameters to the Accessor
        /// </summary>
        internal class SelectEquipmentTypeSizeParameter : IParameterMapper
        {
            private string equipmentsize;
            private string equipmenttype;

            internal SelectEquipmentTypeSizeParameter(string equipmentSize, string equipmentType)
            {
                equipmentsize = equipmentSize;
                equipmenttype = equipmentType;
            }


            #region IParameterMapper Members

            public void AssignParameters(DbCommand command, object[] parameterValues)
            {
                var paramSize = command.CreateParameter();
                paramSize.ParameterName = equipmentsize;
                paramSize.Value = parameterValues[0];
                command.Parameters.Add(paramSize);

                var paramType = command.CreateParameter();
                paramType.ParameterName = equipmenttype;
                paramType.Value = parameterValues[1];
                command.Parameters.Add(paramType);
            }
            #endregion
        }
    }
}

