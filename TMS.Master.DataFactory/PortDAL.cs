﻿using Microsoft.Extensions.Options;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using TMS.Common;
using TMS.Master.Contract;

namespace TMS.Master.DataFactory
{
    public class PortDAL
    {
        private Database db;
        private DbTransaction currentTransaction = null;
        private DbConnection connection = null;
        /// <summary>
        /// Constructor
        /// </summary>
        public PortDAL(IOptions<DataBaseConfiguration> config)
        {
            db = new SqlDatabase(config.Value.dbString);

        }

        #region IDataFactory Members

        public List<Port> GetList()
        {
            return db.ExecuteSprocAccessor(DBRoutine.LISTPORT, MapBuilder<Port>.BuildAllProperties()).ToList();
        }

        public List<Port> GetWharfList()
        {
            return db.ExecuteSprocAccessor(DBRoutine.LISTWHARF, MapBuilder<Port>
                                                .MapAllProperties()
                                                .DoNotMap(p => p.TradeMode)
                                                .DoNotMap(p => p.TradeModeDescription)
                                                .DoNotMap(p => p.AreaPortTypeDescription)
                                                .Build()).ToList();


        }

        public List<Port> GetLocationList()
        {
            return db.ExecuteSprocAccessor(DBRoutine.LISTLOCATION, MapBuilder<Port>
                                                .MapAllProperties()
                                                .DoNotMap(p => p.TradeMode)
                                                .DoNotMap(p => p.TradeModeDescription)
                                                .DoNotMap(p => p.AreaPortTypeDescription)
                                                .Build()).ToList();


        }





        public bool Save<T>(T item) where T : IContract
        {
            var result = 0;

            var port = (Port)(object)item;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();


                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        var savecommand = db.GetStoredProcCommand(DBRoutine.SAVEPORT);

                        db.AddInParameter(savecommand, "PortAreaCode", System.Data.DbType.String, port.PortAreaCode);
                        db.AddInParameter(savecommand, "PortAreaName", System.Data.DbType.String, port.PortAreaName);
                        db.AddInParameter(savecommand, "CountryCode", System.Data.DbType.String, port.CountryCode);
                        db.AddInParameter(savecommand, "AreaPortType", System.Data.DbType.Int16, port.AreaPortType);
                        db.AddInParameter(savecommand, "PostCode", System.Data.DbType.String, port.PostCode);
                        db.AddInParameter(savecommand, "Latitude", System.Data.DbType.String, port.Latitude);
                        db.AddInParameter(savecommand, "Longitude", System.Data.DbType.String, port.Longitude);
                        db.AddInParameter(savecommand, "Status", System.Data.DbType.Boolean, port.Status);
                        db.AddInParameter(savecommand, "ISOCode", System.Data.DbType.String, port.ISOCode);
                        db.AddInParameter(savecommand, "MappingCode", System.Data.DbType.String, port.MappingCode);
                        db.AddInParameter(savecommand, "TradeMode", System.Data.DbType.Int16, port.TradeMode);
                        db.AddInParameter(savecommand, "PaperLessCode", System.Data.DbType.String, port.PaperLessCode);
                        db.AddInParameter(savecommand, "CreatedBy", System.Data.DbType.String, port.CreatedBy);
                        db.AddInParameter(savecommand, "ModifiedBy", System.Data.DbType.String, port.ModifiedBy);
                        //db.AddInParameter(savecommand, "AuditID", System.Data.DbType.String, port.AuditID);



                        result = db.ExecuteNonQuery(savecommand, transaction);

                        transaction.Commit();

                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

            }


            return (result > 0 ? true : false);

        }

        public bool Delete<T>(T item) where T : IContract
        {
            var result = false;
            var port = (Port)(object)item;

            using (DbConnection connnection = db.CreateConnection())
            {
                connnection.Open();
                using (DbTransaction transaction = connnection.BeginTransaction())
                {
                    try
                    {
                        var deleteCommand = db.GetStoredProcCommand(DBRoutine.DELETEPORT);

                        db.AddInParameter(deleteCommand, "PortAreaCode", System.Data.DbType.String, port.PortAreaCode);

                        result = Convert.ToBoolean(db.ExecuteNonQuery(deleteCommand, transaction));

                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }

                }
            }
            return result;
        }

        public IContract GetItem<T>(IContract lookupItem) where T : IContract
        {

            IRowMapper<Port> rowmapper = MapBuilder<Port>.BuildAllProperties();
            IParameterMapper parametermapper = new SelectPortParameter("PortAreaCode");

            var accessor = db.CreateSprocAccessor(DBRoutine.SELECTPORT, parametermapper, rowmapper);
            var portItem = accessor.Execute(((Port)lookupItem).PortAreaCode).FirstOrDefault();
            return portItem;
        }


        public System.Data.IDataReader PerformSearch(string whereClause)
        {

            var searchcommand = db.GetStoredProcCommand(DBRoutine.SEARCHPORT);

            db.AddInParameter(searchcommand, "whereClause", System.Data.DbType.String, whereClause);


            return db.ExecuteReader(searchcommand);


        }

        #endregion


        /// <summary>
        /// private classes for assigning parameters to the Accessor
        /// </summary>
        internal class SelectPortParameter : IParameterMapper
        {
            private string portcode;

            internal SelectPortParameter(string portCode)
            {
                portcode = portCode;
            }


            #region IParameterMapper Members

            public void AssignParameters(DbCommand command, object[] parameterValues)
            {
                var parameter = command.CreateParameter();
                parameter.ParameterName = "PortAreaCode";
                parameter.Value = parameterValues[0];
                command.Parameters.Add(parameter);
            }

            #endregion
        }





    }
}
