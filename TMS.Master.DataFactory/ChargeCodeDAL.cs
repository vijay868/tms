﻿using System;
using System.Collections.Generic;
using System.Data.Common;

using Microsoft.Extensions.Options;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using TMS.Common;
using TMS.Master.Contract;
using System.Linq;

namespace TMS.Master.DataFactory
{
    public class ChargeCodeDAL
    {
        private Database db;
        private DbTransaction currentTransaction = null;
        private DbConnection connection = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public ChargeCodeDAL(IOptions<DataBaseConfiguration> config)
        {
            db = new SqlDatabase(config.Value.dbString);

        }

        #region IDataFactory Members

        public List<ChargeCodeMaster> GetList()
        {
            return db.ExecuteSprocAccessor(DBRoutine.LISTCHARGECODE, MapBuilder<ChargeCodeMaster>.BuildAllProperties()).ToList();
        }

        public bool Save<T>(T item) where T : IContract
        {
            var result = 0;

            var chargecodemaster = (ChargeCodeMaster)(object)item;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();


                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        var savecommand = db.GetStoredProcCommand(DBRoutine.SAVECHARGECODE);

                        db.AddInParameter(savecommand, "ChargeCode", System.Data.DbType.String, chargecodemaster.ChargeCode);
                        db.AddInParameter(savecommand, "Description", System.Data.DbType.String, chargecodemaster.Description);
                        db.AddInParameter(savecommand, "Module", System.Data.DbType.Int16, chargecodemaster.Module);
                        db.AddInParameter(savecommand, "ChargeType", System.Data.DbType.Int16, chargecodemaster.ChargeType);
                        db.AddInParameter(savecommand, "BillingUnit", System.Data.DbType.Int16, chargecodemaster.BillingUnit);
                        db.AddInParameter(savecommand, "Status", System.Data.DbType.Boolean, chargecodemaster.Status);
                        db.AddInParameter(savecommand, "VAT", System.Data.DbType.Decimal, chargecodemaster.VAT);
                        db.AddInParameter(savecommand, "CreditTerm", System.Data.DbType.Decimal, chargecodemaster.CreditTerm);
                        db.AddInParameter(savecommand, "PaymentTerm", System.Data.DbType.Int16, chargecodemaster.PaymentTerm);
                        db.AddInParameter(savecommand, "ChargeTerm", System.Data.DbType.Int16, chargecodemaster.ChargeTerm);
                        db.AddInParameter(savecommand, "CreatedBy", System.Data.DbType.String, chargecodemaster.CreatedBy);
                        db.AddInParameter(savecommand, "ModifiedBy", System.Data.DbType.String, chargecodemaster.ModifiedBy);
                        db.AddInParameter(savecommand, "IsByService", System.Data.DbType.Boolean, chargecodemaster.IsByService);

                        //db.AddInParameter(savecommand, "AuditID", System.Data.DbType.String, chargecodemaster.AuditID);


                        result = db.ExecuteNonQuery(savecommand, transaction);

                        transaction.Commit();

                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

            }


            return (result > 0 ? true : false);

        }

        public bool Delete<T>(T item) where T : IContract
        {
            var result = false;
            var chargecodemaster = (ChargeCodeMaster)(object)item;

            var connnection = db.CreateConnection();
            connnection.Open();
            var transaction = connnection.BeginTransaction();

            try
            {
                var deleteCommand = db.GetStoredProcCommand(DBRoutine.DELETECHARGECODE);

                db.AddInParameter(deleteCommand, "ChargeCode", System.Data.DbType.String, chargecodemaster.ChargeCode);

                result = Convert.ToBoolean(db.ExecuteNonQuery(deleteCommand, transaction));

                transaction.Commit();

            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }

            return result;
        }

        public IContract GetItem<T>(IContract lookupItem) where T : IContract
        {

            IRowMapper<ChargeCodeMaster> rowmapper = MapBuilder<ChargeCodeMaster>.BuildAllProperties();
            IParameterMapper parametermapper = new SelectChargeCodeParameter("ChargeCode");

            var accessor = db.CreateSprocAccessor(DBRoutine.SELECTCHARGECODE, parametermapper, rowmapper);
            var chargecodemasterItem = accessor.Execute(((ChargeCodeMaster)lookupItem).ChargeCode).FirstOrDefault();
            return chargecodemasterItem;
        }



        public System.Data.IDataReader PerformSearch(string whereClause)
        {

            var searchcommand = db.GetStoredProcCommand(DBRoutine.SEARCHCHARGECODE);

            db.AddInParameter(searchcommand, "whereClause", System.Data.DbType.String, whereClause);

            //var tempReader = (SqlDataReader)db.ExecuteReader(searchcommand);
            //return (SqlDataReader)tempReader.InnerReader;

            return db.ExecuteReader(searchcommand);


        }

        #endregion


        /// <summary>
        /// private classes for assigning parameters to the Accessor
        /// </summary>
        internal class SelectChargeCodeParameter : IParameterMapper
        {
            private string parameterName;
            internal SelectChargeCodeParameter(string ParameterName)
            {
                parameterName = ParameterName;
            }


            #region IParameterMapper Members

            public void AssignParameters(DbCommand command, object[] parameterValues)
            {
                var parameter = command.CreateParameter();
                parameter.ParameterName = parameterName;
                parameter.Value = parameterValues[0];
                command.Parameters.Add(parameter);
            }

            #endregion
        }
    }
}
