﻿using System;
using System.Collections.Generic;
using System.Data.Common;

using Microsoft.Extensions.Options;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using TMS.Common;
using TMS.Master.Contract;
using System.Linq;

namespace TMS.Master.DataFactory
{
    public class VesselDAL
    {
        private Database db;
        private DbTransaction currentTransaction = null;
        private DbConnection connection = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public VesselDAL(IOptions<DataBaseConfiguration> config)
        {
            db = new SqlDatabase(config.Value.dbString);

        }
        #region IDataFactory Members

        public List<Vessel> GetList()
        {
            return db.ExecuteSprocAccessor(DBRoutine.LISTVESSEL, MapBuilder<Vessel>.BuildAllProperties()).ToList();
        }

        public bool Save<T>(T item) where T : IContract
        {
            var result = 0;

            var vessel = (Vessel)(object)item;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();


                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        var savecommand = db.GetStoredProcCommand(DBRoutine.SAVEVESSEL);

                        db.AddInParameter(savecommand, "VesselCode", System.Data.DbType.String, vessel.VesselCode);
                        db.AddInParameter(savecommand, "VesselName", System.Data.DbType.String, vessel.VesselName);
                        db.AddInParameter(savecommand, "CallSign", System.Data.DbType.String, vessel.CallSign);
                        db.AddInParameter(savecommand, "MappingCode", System.Data.DbType.String, vessel.MappingCode);
                        db.AddInParameter(savecommand, "CreatedBy", System.Data.DbType.String, vessel.CreatedBy);
                        db.AddInParameter(savecommand, "ModifiedBy", System.Data.DbType.String, vessel.ModifiedBy);
                        db.AddInParameter(savecommand, "Status", System.Data.DbType.Boolean, vessel.Status);

                        result = db.ExecuteNonQuery(savecommand, transaction);

                        transaction.Commit();

                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

            }


            return (result > 0 ? true : false);

        }

        public bool Delete<T>(T item) where T : IContract
        {
            var result = false;
            var vessel = (Vessel)(object)item;

            using (DbConnection connnection = db.CreateConnection())
            {
                connnection.Open();
                using (DbTransaction transaction = connnection.BeginTransaction())
                {
                    try
                    {
                        var deleteCommand = db.GetStoredProcCommand(DBRoutine.DELETEVESSEL);

                        db.AddInParameter(deleteCommand, "VesselCode", System.Data.DbType.String, vessel.VesselCode);

                        result = Convert.ToBoolean(db.ExecuteNonQuery(deleteCommand, transaction));

                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }

                }
            }
            return result;
        }

        public IContract GetItem<T>(IContract lookupItem) where T : IContract
        {

            IRowMapper<Vessel> rowmapper = MapBuilder<Vessel>.BuildAllProperties();
            IParameterMapper parametermapper = new SelectVesselParameter("VesselCode");

            var accessor = db.CreateSprocAccessor(DBRoutine.SELECTVESSEL, parametermapper, rowmapper);
            var vesselItem = accessor.Execute(((Vessel)lookupItem).VesselCode).FirstOrDefault();
            return vesselItem;
        }


        public System.Data.IDataReader PerformSearch(string whereClause)
        {

            var searchcommand = db.GetStoredProcCommand(DBRoutine.SEARCHVESSEL);

            db.AddInParameter(searchcommand, "whereClause", System.Data.DbType.String, whereClause);


            return db.ExecuteReader(searchcommand);


        }

        #endregion


        /// <summary>
        /// private classes for assigning parameters to the Accessor
        /// </summary>
        internal class SelectVesselParameter : IParameterMapper
        {
            private string vesselcode;

            internal SelectVesselParameter(string vesselCode)
            {
                vesselcode = vesselCode;
            }

            #region IParameterMapper Members

            public void AssignParameters(DbCommand command, object[] parameterValues)
            {
                var parameter = command.CreateParameter();
                parameter.ParameterName = "VesselCode";
                parameter.Value = parameterValues[0];
                command.Parameters.Add(parameter);
            }

            #endregion
        }



    }
}
