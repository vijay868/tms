﻿using Microsoft.Extensions.Options;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using TMS.Common;
using TMS.Master.Contract;

namespace TMS.Master.DataFactory
{
    public class ContainerStatusDAL
    {
        private Database db;
        private DbTransaction currentTransaction = null;
        private DbConnection connection = null;

        /// <summary>
        /// Constructor
        /// </summary>
        public ContainerStatusDAL(IOptions<DataBaseConfiguration> config)
        {
            db = new SqlDatabase(config.Value.dbString);
        }

        #region IDataFactory Members

        public List<ContainerStatus> GetList()
        {
            return db.ExecuteSprocAccessor(DBRoutine.LISTCONTAINERSTATUS, MapBuilder<ContainerStatus>.BuildAllProperties()).ToList();
        }


        public bool SaveList<T>(List<T> items, DbTransaction parentTransaction) where T : IContract
        {
            var result = true;

            if (items.Count == 0)
                result = true;

            foreach (var item in items)
            {
                result = Save(item, parentTransaction);
                if (result == false) break;
            }


            return result;

        }

        public bool Save<T>(T item, DbTransaction parentTransaction) where T : IContract
        {
            currentTransaction = parentTransaction;
            return Save(item);

        }



        public bool Save<T>(T item) where T : IContract
        {
            var result = 0;

            var containerstatus = (ContainerStatus)(object)item;

            if (currentTransaction == null || currentTransaction.Connection == null)
            {
                connection = db.CreateConnection();
                connection.Open();
            }

            var transaction = (currentTransaction == null ? connection.BeginTransaction() : currentTransaction);

            try
            {
                var savecommand = db.GetStoredProcCommand(DBRoutine.SAVECONTAINERSTATUS);

                db.AddInParameter(savecommand, "Code", System.Data.DbType.String, containerstatus.Code);
                db.AddInParameter(savecommand, "Description", System.Data.DbType.String, containerstatus.Description);
                db.AddInParameter(savecommand, "IsHold", System.Data.DbType.Boolean, containerstatus.IsHold);
                db.AddInParameter(savecommand, "Status", System.Data.DbType.Boolean, containerstatus.Status);
                db.AddInParameter(savecommand, "CreatedBy", System.Data.DbType.String, containerstatus.CreatedBy);
                db.AddInParameter(savecommand, "ModifiedBy", System.Data.DbType.String, containerstatus.ModifiedBy);



                result = db.ExecuteNonQuery(savecommand, transaction);

                if (currentTransaction == null)
                    transaction.Commit();

            }
            catch (Exception)
            {
                if (currentTransaction == null)
                    transaction.Rollback();

                throw;
            }

            return (result > 0 ? true : false);

        }


        public bool Delete(string containerno, DbTransaction parentTransaction)
        {
            var containerfixportItem = new ContainerFixPort { ContainerNo = containerno };

            return Delete(containerfixportItem, parentTransaction);
        }

        public bool Delete<T>(T item, DbTransaction parentTransaction) where T : IContract
        {
            currentTransaction = parentTransaction;
            return Delete(item);
        }





        public bool Delete<T>(T item) where T : IContract
        {
            var result = false;
            var containerstatus = (ContainerStatus)(object)item;

            if (currentTransaction == null || currentTransaction.Connection == null)
            {
                connection = db.CreateConnection();
                connection.Open();
            }

            var transaction = (currentTransaction == null ? connection.BeginTransaction() : currentTransaction);

            try
            {
                var deleteCommand = db.GetStoredProcCommand(DBRoutine.DELETECONTAINERSTATUS);

                db.AddInParameter(deleteCommand, "Code", System.Data.DbType.String, containerstatus.Code);
                db.AddInParameter(deleteCommand, "ModifiedBy", System.Data.DbType.String, containerstatus.ModifiedBy);

                result = Convert.ToBoolean(db.ExecuteNonQuery(deleteCommand, transaction));

                if (currentTransaction == null)
                    transaction.Commit();

            }
            catch (Exception ex)
            {
                if (currentTransaction == null)
                    transaction.Rollback();
                throw ex;
            }

            return result;
        }

        public IContract GetItem<T>(IContract lookupItem) where T : IContract
        {


            var containersatusItem = db.ExecuteSprocAccessor(DBRoutine.SELECTCONTAINERSTATUS,
                                                    MapBuilder<ContainerStatus>.BuildAllProperties(),
                                                    ((ContainerStatus)lookupItem).Code).FirstOrDefault();
            return containersatusItem;
        }

        #endregion






    }
}
