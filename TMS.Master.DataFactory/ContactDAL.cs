﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;

using Microsoft.Extensions.Options;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using TMS.Common;
using TMS.Master.Contract;
using System.Linq;

namespace TMS.Master.DataFactory
{
    public class ContactDAL
    {
        private Database db;
        private DbTransaction currentTransaction = null;
        private DbConnection connection = null;
        /// <summary>
        /// Constructor
        /// </summary>
        public ContactDAL(IOptions<DataBaseConfiguration> config)
        {
            db = new SqlDatabase(config.Value.dbString);

        }

        #region IDataFactory Members

        public List<Contact> GetList()
        {
            return db.ExecuteSprocAccessor(DBRoutine.LISTCONTACT, MapBuilder<Contact>.BuildAllProperties()).ToList();
        }

        public bool Save<T>(T item, DbTransaction parentTransaction) where T : IContract
        {
            currentTransaction = parentTransaction;
            return Save(item);

        }


        public bool SaveList<T>(List<T> items, DbTransaction parentTransaction) where T : IContract
        {
            var result = true;

            if (items.Count == 0)
                result = true;

            foreach (var item in items)
            {
                result = Save(item, parentTransaction);
                if (result == false) break;
            }


            return result;

        }


        public bool Save<T>(T item) where T : IContract
        {
            var result = 0;

            var contact = (Contact)(object)item;

            if (currentTransaction == null)
            {
                connection = db.CreateConnection();
                connection.Open();
            }

            var transaction = (currentTransaction == null ? connection.BeginTransaction() : currentTransaction);

            try
            {

                var savecommand = db.GetStoredProcCommand(DBRoutine.SAVECONTACT);

                db.AddInParameter(savecommand, "ContactID", System.Data.DbType.Int64, contact.ContactID);
                db.AddInParameter(savecommand, "AddressLinkID", System.Data.DbType.String, contact.AddressLinkID);
                db.AddInParameter(savecommand, "EntityType", System.Data.DbType.String, contact.EntityType);
                db.AddInParameter(savecommand, "Address1", System.Data.DbType.String, contact.Address1);
                db.AddInParameter(savecommand, "Address2", System.Data.DbType.String, contact.Address2);
                db.AddInParameter(savecommand, "State", System.Data.DbType.String, contact.State);
                db.AddInParameter(savecommand, "CountryCode", System.Data.DbType.String, contact.CountryCode);
                db.AddInParameter(savecommand, "PostCode", System.Data.DbType.String, contact.PostCode);
                db.AddInParameter(savecommand, "PhoneNumber", System.Data.DbType.String, contact.PhoneNumber);
                db.AddInParameter(savecommand, "FaxNumber", System.Data.DbType.String, contact.FaxNumber);
                db.AddInParameter(savecommand, "ContactPerson", System.Data.DbType.String, contact.ContactPerson);
                db.AddInParameter(savecommand, "MobilePhoneNumber", System.Data.DbType.String, contact.MobilePhoneNumber);
                db.AddInParameter(savecommand, "EmailID", System.Data.DbType.String, contact.EmailID);
                db.AddInParameter(savecommand, "WebSite", System.Data.DbType.String, contact.WebSite);
                db.AddInParameter(savecommand, "Social1", System.Data.DbType.String, contact.Social1);
                db.AddInParameter(savecommand, "Social2", System.Data.DbType.String, contact.Social2);
                db.AddInParameter(savecommand, "IsDefault", System.Data.DbType.Boolean, contact.IsDefault);
                db.AddInParameter(savecommand, "Status", System.Data.DbType.Boolean, contact.Status);
                db.AddInParameter(savecommand, "IsNewRecord", System.Data.DbType.Boolean, contact.IsNewRecord);




                result = db.ExecuteNonQuery(savecommand, transaction);

                if (currentTransaction == null)
                    transaction.Commit();

            }
            catch (Exception)
            {
                if (currentTransaction == null)
                    transaction.Rollback();

                throw;
            }





            return (result > 0 ? true : false);

        }

        public bool Delete<T>(T item) where T : IContract
        {
            var result = false;
            var contact = (Contact)(object)item;

            using (DbConnection connnection = db.CreateConnection())
            {
                connnection.Open();
                using (DbTransaction transaction = connnection.BeginTransaction())
                {
                    try
                    {
                        var deleteCommand = db.GetStoredProcCommand(DBRoutine.DELETECONTACT);

                        db.AddInParameter(deleteCommand, "ContactID", System.Data.DbType.Int64, contact.ContactID);

                        result = Convert.ToBoolean(db.ExecuteNonQuery(deleteCommand, transaction));

                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }

                }
            }
            return result;
        }


        public List<Contact> GetContactsByCustomer(IContract lookupItem)
        {
            //return db.ExecuteSprocAccessor(DBRoutine.LISTCONTACTBYCUSTOMER,
            //                               MapBuilder<Contact>.BuildAllProperties(),
            //                               ((Contact)lookupItem).AddressLinkID,
            //                               ((Contact)lookupItem).EntityType).ToList();


            return db.ExecuteSprocAccessor(DBRoutine.LISTCONTACTBYCUSTOMER,
                                                      MapBuilder<Contact>
                                                      .MapAllProperties()
                                                      .DoNotMap(c => c.Selected)
                                                      .Build(),
                                                      ((Contact)lookupItem).AddressLinkID,
                                                      ((Contact)lookupItem).EntityType).ToList();




        }

        public IContract GetItem<T>(IContract lookupItem) where T : IContract
        {
            throw new NotImplementedException();
        }


        public Contact GetItem(Master.Contract.Contact lookupItem)
        {

            IRowMapper<Contact> rowmapper = MapBuilder<Contact>.BuildAllProperties();
            IParameterMapper parametermapper = new SelectContactParameter("ContactID");

            var accessor = db.CreateSprocAccessor(DBRoutine.SELECTCONTACT, parametermapper, rowmapper);
            var contactItem = accessor.Execute(((Contact)lookupItem).ContactID).FirstOrDefault();
            return contactItem;
        }



        #endregion


        /// <summary>
        /// private classes for assigning parameters to the Accessor
        /// </summary>
        internal class SelectContactParameter : IParameterMapper
        {

            private string paramname;

            internal SelectContactParameter(string paramName)
            {
                paramname = paramName;
            }

            #region IParameterMapper Members

            public void AssignParameters(DbCommand command, object[] parameterValues)
            {
                var parameter = command.CreateParameter();
                parameter.ParameterName = paramname;
                parameter.Value = parameterValues[0];
                command.Parameters.Add(parameter);
            }

            #endregion
        }
    }
}
