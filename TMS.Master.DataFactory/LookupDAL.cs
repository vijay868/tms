﻿using Microsoft.Extensions.Options;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using TMS.Common;
using TMS.Master.Contract;
namespace TMS.Master.DataFactory
{
    public class LookupDAL
    {
        private Database db;        
        /// <summary>
        /// Constructor
        /// </summary>
        public LookupDAL(IOptions<DataBaseConfiguration> config)
        {
            db = new SqlDatabase(config.Value.dbString);

        }

        public List<Lookup> GetListByCategory(string lookupCategory = "")
        {

            return db.ExecuteSprocAccessor(DBRoutine.LISTLOOKUPBYCATEGORY, MapBuilder<Lookup>.BuildAllProperties(), lookupCategory).ToList();
        }

        public List<Lookup> GetLookupList()
        {
            return db.ExecuteSprocAccessor(DBRoutine.LISTLOOKUP, MapBuilder<Lookup>.BuildAllProperties()).ToList();
        }

        public List<Lookup> GetLookupCategories()
        {

            IRowMapper<Lookup> rowmapper = MapBuilder<Lookup>.MapNoProperties().Map(c => c.LookupCategory).ToColumn("LookupCategory").Build();

            var lstCategories = db.ExecuteSprocAccessor(DBRoutine.LISTCATEGORY, rowmapper).ToList();

            return lstCategories;
        }


        public bool Save<T>(T item) where T : IContract
        {
            var result = 0;

            var lookup = (Lookup)(object)item;

            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();


                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        var savecommand = db.GetStoredProcCommand(DBRoutine.SAVELOOKUP);

                        db.AddInParameter(savecommand, "LookupID", System.Data.DbType.Int16, lookup.LookupID);
                        db.AddInParameter(savecommand, "LookupCode", System.Data.DbType.String, lookup.LookupCode);
                        db.AddInParameter(savecommand, "LookupDescription", System.Data.DbType.String, lookup.LookupDescription);
                        db.AddInParameter(savecommand, "LookupCategory", System.Data.DbType.String, lookup.LookupCategory);
                        db.AddInParameter(savecommand, "Status", System.Data.DbType.Boolean, lookup.Status);
                        db.AddInParameter(savecommand, "ISOCode", System.Data.DbType.String, lookup.ISOCode);
                        db.AddInParameter(savecommand, "MappingCode", System.Data.DbType.String, lookup.MappingCode);
                        db.AddInParameter(savecommand, "CreatedBy", System.Data.DbType.String, lookup.CreatedBy);
                        db.AddInParameter(savecommand, "ModifiedBy", System.Data.DbType.String, lookup.ModifiedBy);


                        result = db.ExecuteNonQuery(savecommand, transaction);


                        //saveDetails(transaction);



                        transaction.Commit();

                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

            }


            return (result > 0 ? true : false);

        }

        public bool Delete<T>(T item) where T : IContract
        {
            var result = false;
            var lookup = (Lookup)(object)item;

            using (DbConnection connnection = db.CreateConnection())
            {
                connnection.Open();
                using (DbTransaction transaction = connnection.BeginTransaction())
                {
                    try
                    {
                        var deleteCommand = db.GetStoredProcCommand(DBRoutine.DELETELOOKUP);

                        db.AddInParameter(deleteCommand, "LookupID", System.Data.DbType.Int16, lookup.LookupID);

                        result = Convert.ToBoolean(db.ExecuteNonQuery(deleteCommand, transaction));

                        transaction.Commit();

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }

                }
            }
            return result;
        }

        public IContract GetItem<T>(IContract lookupItem) where T : IContract
        {

            IRowMapper<Lookup> rowmapper = MapBuilder<Lookup>.BuildAllProperties();
            IParameterMapper parametermapper = new SelectLookupParameter("LookupCode", "LookupCategory");

            var accessor = db.CreateSprocAccessor(DBRoutine.SELECTLOOKUP, parametermapper, rowmapper);
            var Item = accessor.Execute(((Lookup)lookupItem).LookupCode, ((Lookup)lookupItem).LookupCategory).FirstOrDefault();
            return Item;
        }

        /// <summary>
        /// private classes for assigning parameters to the Accessor
        /// </summary>
        internal class SelectLookupParameter : IParameterMapper
        {
            private string lookupcode;
            private string lookupcategory;

            internal SelectLookupParameter(string lookupCode, string lookupCategory)
            {
                lookupcode = lookupCode;
                lookupcategory = lookupCategory;
            }


            #region IParameterMapper Members

            public void AssignParameters(DbCommand command, object[] parameterValues)
            {
                var paramlookupCode = command.CreateParameter();
                paramlookupCode.ParameterName = "LookupCode";
                paramlookupCode.Value = parameterValues[0];
                command.Parameters.Add(paramlookupCode);

                var paramlookupCat = command.CreateParameter();
                paramlookupCat.ParameterName = "LookupCategory";
                paramlookupCat.Value = parameterValues[1];
                command.Parameters.Add(paramlookupCat);

            }

            #endregion
        }
    }
}
