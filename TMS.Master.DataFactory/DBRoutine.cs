﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TMS.Master.DataFactory
{
    public static class DBRoutine
    {

        /// <summary>
        /// StoredProcedures related to database table(s).
        /// all the constant variables uses CAPITAL CASING
        /// </summary>


        /// <summary>
        /// [Master].[Country]
        /// </summary>
        public const string LISTCOUNTRY = "[Master].[usp_CountyList]";
        public const string SAVECOUNTRY = "[Master].[usp_CountrySave]";
        public const string DELETECOUNTRY = "[Master].[usp_CountryDelete]";
        public const string SELECTCOUNTRY = "[Master].[usp_CountrySelect]";
        public const string SEARCHCOUNTRY = "[Master].[usp_PerformCountySearch]";



        /// <summary>
        /// [Master].[Branch]
        /// </summary>
        public const string DELETEBRANCH = "[Master].[usp_BranchDelete]";
        public const string LISTBRANCH = "[Master].[usp_BranchList]";
        public const string LISTBRANCHBYCOMPANY = "[Master].[usp_BranchListByCompany]";
        public const string SAVEBRANCH = "[Master].[usp_BranchSave]";
        public const string SELECTBRANCH = "[Master].[usp_BranchSelect]";


        /// <summary>
        /// [Master].[Company]
        /// </summary>
        public const string DELETECOMPANY = "[Master].[usp_CompanyDelete]";
        public const string LISTCOMPANY = "[Master].[usp_CompanyList]";
        public const string SAVECOMPANY = "[Master].[usp_CompanySave]";
        public const string SELECTCOMPANY = "[Master].[usp_CompanySelect]";


        /// <summary>
        /// [Master].[Contact]
        /// </summary>

        public const string DELETECONTACT = "[Master].[usp_ContactDelete]";
        public const string LISTCONTACT = "[Master].[usp_ContactList]";
        public const string SAVECONTACT = "[Master].[usp_ContactSave]";
        public const string SELECTCONTACT = "[Master].[usp_ContactSelect]";
        public const string LISTCONTACTBYCUSTOMER = "[Master].[usp_ContactListByCustomer]";



        /// <summary>
        /// [Master].[ChargeCode]
        /// </summary>
        public const string DELETECHARGECODE = "[Master].[usp_ChargeCodeDelete]";
        public const string LISTCHARGECODE = "[Master].[usp_ChargeCodeList]";
        public const string SAVECHARGECODE = "[Master].[usp_ChargeCodeSave]";
        public const string SELECTCHARGECODE = "[Master].[usp_ChargeCodeSelect]";
        public const string SEARCHCHARGECODE = "[Master].[usp_PerformChargeCodeSearch]";


        /// <summary>
        /// [Master].[Container]
        /// </summary>

        public const string DELETECONTAINER = "[Master].[usp_ContainerDelete]";
        public const string LISTCONTAINER = "[Master].[usp_ContainerList]";
        public const string SAVECONTAINER = "[Master].[usp_ContainerSave]";
        public const string SELECTCONTAINER = "[Master].[usp_ContainerSelect]";
        public const string SEARCHCONTAINER = "[Master].[usp_PerformContainerSearch]";
        public const string CHANGECONTAINERSTATUS = "[Master].[usp_ChangeContainerStatus]";
        public const string CONTAINERYARDTRANSFER = "[Master].[usp_ContainerYardTransfer]";
        public const string RETRIEVECONTAINERFORWORKORDER = "[Master].[usp_RetrieveContainerForWorkOrder]";

        public const string CONTAINERINFOUPDATE = "[Master].[usp_ContainerInfoUpdate]";


        public const string CONTAINERSELECTFORYARDMAP = "[Master].[usp_ContainerSelectForYardMap]";




        /// <summary>
        /// [Master].[Customer]
        /// </summary>

        public const string DELETECUSTOMER = "[Master].[usp_CustomerDelete]";
        public const string LISTCUSTOMER = "[Master].[usp_CustomerList]";
        public const string SAVECUSTOMER = "[Master].[usp_CustomerSave]";
        public const string SELECTCUSTOMER = "[Master].[usp_CustomerSelect]";
        public const string SEARCHCUSTOMER = "[Master].[usp_PerformCustomerSearch]";
        public const string SELECTCUSTOMERBYNAME = "[Master].[usp_SelectCustomerByName]";
        public const string LISTAGENT = "[Master].[usp_AgentList]";



        /// <summary>
        /// [Config].[LookUp]
        /// </summary>


        public const string DELETELOOKUP = "[Config].[usp_LookupDelete]";
        public const string LISTLOOKUPBYCATEGORY = "[Config].[usp_LookupListByCategory]";
        public const string SAVELOOKUP = "[Config].[usp_LookupSave]";
        public const string SELECTLOOKUP = "[Config].[usp_LookupSelect]";
        public const string LISTLOOKUP = "[Config].[usp_LookupList]";
        public const string LISTCATEGORY = "[Config].[usp_LookupListCategoryType]";



        /// <summary>
        /// [Master].[Movement]
        /// </summary>


        public const string DELETEMOVEMENT = "[Master].[usp_MovementDelete]";
        public const string LISTMOVEMENT = "[Master].[usp_MovementList]";
        public const string SAVEMOVEMENT = "[Master].[usp_MovementSave]";
        public const string SELECTMOVEMENT = "[Master].[usp_MovementSelect]";
        public const string LISTMOVEMENTBYMODULE = "[Master].[usp_MovementListByModule]";




        /// <summary>
        /// [Master].[Port]
        /// </summary>

        public const string DELETEPORT = "[Master].[usp_PortDelete]";
        public const string LISTPORT = "[Master].[usp_PortList]";
        public const string SAVEPORT = "[Master].[usp_PortSave]";
        public const string SELECTPORT = "[Master].[usp_PortSelect]";
        public const string LISTWHARF = "[Master].[usp_WharfList]";
        public const string SEARCHPORT = "[Master].[usp_PerformPortSearch]";
        public const string LISTLOCATION = "[Master].[usp_AreaPortLocationList]";


        /// <summary>
        /// [Master].[Yard]
        /// </summary>
        public const string DELETEYARD = "[Master].[usp_YardDelete]";
        public const string LISTYARD = "[Master].[usp_YardList]";
        public const string SAVEYARD = "[Master].[usp_YardSave]";
        public const string SELECTYARD = "[Master].[usp_YardSelect]";

        /// <summary>
        /// [Master].[IMOCode]
        /// </summary>

        public const string DELETEIMOCODE = "[Master].[usp_IMOCodeDelete]";
        public const string SELECTIMOCODE = "[Master].[usp_IMOCodeSelect]";
        public const string LISTIMO = "[Master].[usp_IMOList]";
        public const string SAVEIMO = "[Master].[usp_IMOSave]";




        /// <summary>
        /// [Master].[Vessel]
        /// </summary>

        public const string DELETEVESSEL = "[Master].[usp_VesselDelete]";
        public const string LISTVESSEL = "[Master].[usp_VesselList]";
        public const string SAVEVESSEL = "[Master].[usp_VesselSave]";
        public const string SELECTVESSEL = "[Master].[usp_VesselSelect]";
        public const string SEARCHVESSEL = "[Master].[usp_PerformVesselSearch]";


        /// <summary>
        /// [Master].[PUDOMaster]
        /// </summary>

        public const string DELETEPUDOMASTER = "[Master].[usp_PUDOMasterDelete]";
        public const string LISTPUDOMASTER = "[Master].[usp_PUDOMasterList]";
        public const string SAVEPUDOMASTER = "[Master].[usp_PUDOMasterSave]";
        public const string SELECTPUDOMASTER = "[Master].[usp_PUDOMasterSelect]";
        public const string LISTPUDOBYORDERTYPEANDPUMODE = "[Master].[usp_ListPUDOByOrderTypeAndPUMode]";
        public const string LISTPUDOBYORDERTYPE = "[Master].[usp_ListPUDOByOrderType]";

        /// <summary>
        /// [Master].[HaulierChargeTerm]
        /// </summary>

        public const string DELETEHAULIERCHARGETERM = "[Master].[usp_HaulierChargeTermDelete]";
        public const string LISTHAULIERCHARGETERM = "[Master].[usp_HaulierChargeTermList]";
        public const string SAVEHAULIERCHARGETERM = "[Master].[usp_HaulierChargeTermSave]";
        public const string SELECTPAYMENTTERMFORHAULIER = "[Master].[usp_GetPaymentTermForHaulier]";
        public const string LISTHAULIERORDERCHARGES = "[Master].[usp_GetPaymentTermForHaulierAndOrderType]";

        /// <summary>
        /// [Master].[ContainerFixPort]
        /// </summary>

        public const string DELETECONTAINERFIXPORT = "[Master].[usp_ContainerFixPortDelete]";
        public const string LISTCONTAINERFIXPORT = "[Master].[usp_ContainerFixPortList]";
        public const string SAVECONTAINERFIXPORT = "[Master].[usp_ContainerFixPortSave]";
        public const string SELECTCONTAINERFIXPORT = "[Master].[usp_ContainerFixPortSelect]";



        /// <summary>
        /// [Master].[ContainerStatus]
        /// </summary>

        public const string DELETECONTAINERSTATUS = "[Master].[usp_ContainerStatusDelete]";
        public const string LISTCONTAINERSTATUS = "[Master].[usp_ContainerStatusList]";
        public const string SAVECONTAINERSTATUS = "[Master].[usp_ContainerStatusSave]";
        public const string SELECTCONTAINERSTATUS = "[Master].[usp_ContainerStatusSelect]";



        /// <summary>
        /// [Security].[Users]
        /// </summary>

        public const string DELETEUSERS = "[Security].[usp_UsersDelete]";
        public const string LISTUSERS = "[Security].[usp_UsersList]";
        public const string SAVEUSERS = "[Security].[usp_UsersSave]";
        public const string SELECTUSERS = "[Security].[usp_UsersSelect]";
        public const string VALIDATEUSERLOGIN = "[Security].[usp_ValidateUserLogin]";
        public const string CHANGEUSERPASSWORD = "[Security].[usp_ChangeUserPassword]";



        /// <summary>
        /// [Security].[UserSites]
        /// </summary>

        public const string DELETEUSERSITES = "[Security].[usp_UserSitesDelete]";
        public const string LISTUSERSITES = "[Security].[usp_UserSitesList]";
        public const string SAVEUSERSITES = "[Security].[usp_UserSitesSave]";
        public const string SELECTUSERSITES = "[Security].[usp_UserSitesSelect]";
        public const string REMOVEUSERSITES = "[Security].[usp_UserSitesRemoveAll]";



        /// <summary>
        /// [Config].[EquipmentTypeSize]
        /// </summary>

        public const string DELETEEQUIPMENTTYPESIZE = "[Config].[usp_EquipmentTypeSizeDelete]";
        public const string LISTEQUIPMENTTYPESIZE = "[Config].[usp_EquipmentTypeSizeList]";
        public const string SAVEEQUIPMENTTYPESIZE = "[Config].[usp_EquipmentTypeSizeSave]";
        public const string SELECTEQUIPMENTTYPESIZE = "[Config].[usp_EquipmentTypeSizeSelect]";


        /// <summary>
        /// [Master].[AgentChargeCodeMappi]
        /// </summary>

        public const string DELETEAGENTCHARGECODEMAPPING = "[Master].[usp_AgentChargeCodeMappingDelete]";
        public const string LISTAGENTCHARGECODEMAPPING = "[Master].[usp_AgentChargeCodeMappingList]";
        public const string SAVEAGENTCHARGECODEMAPPING = "[Master].[usp_AgentChargeCodeMappingSave]";
        public const string SELECTAGENTCHARGECODEMAPPING = "[Master].[usp_AgentChargeCodeMappingSelect]";




        /// <summary>
        /// [Security].[UserRights]
        /// </summary>

        public const string DELETEUSERRIGHTS = "[Security].[usp_UserRightsDelete]";
        public const string LISTUSERRIGHTS = "[Security].[usp_UserRightsList]";
        public const string SAVEUSERRIGHTS = "[Security].[usp_UserRightsSave]";
        public const string SELECTUSERRIGHTS = "[Security].[usp_UserRightsSelect]";
        public const string REMOVEUSERRIGHTS = "[Security].[usp_UserRightsRemoveAll]";



        /// <summary>
        /// [Security].[Roles]
        /// </summary>

        public const string DELETEROLES = "[Security].[usp_RolesDelete]";
        public const string LISTROLES = "[Security].[usp_RolesList]";
        public const string SAVEROLES = "[Security].[usp_RolesSave]";
        public const string SELECTROLES = "[Security].[usp_RolesSelect]";


        /// <summary>
        /// [Security].[Securables]
        /// </summary>

        public const string DELETESECURABLES = "[Security].[usp_SecurablesDelete]";
        public const string LISTSECURABLES = "[Security].[usp_SecurablesList]";
        public const string SAVESECURABLES = "[Security].[usp_SecurablesSave]";
        public const string SELECTSECURABLES = "[Security].[usp_SecurablesSelect]";


        public const string GETAPPLICATIONVERSION = "[Config].[usp_GetApplicationVersion]";
        public const string UPDATEAPPLICATIONVERSION = "[Config].[usp_SystemWideConfigurationSave]";

        public const string GETCONFIGURATIONVALUE = "[Config].[usp_SystemWideConfigurationSelect]";



        /// <summary>
        /// [Config].[SystemWideConfiguration]
        /// </summary>

        public const string DELETESYSTEMWIDECONFIGURATION = "[Config].[usp_SystemWideConfigurationDelete]";
        public const string LISTSYSTEMWIDECONFIGURATION = "[Config].[usp_SystemWideConfigurationList]";
        public const string SAVESYSTEMWIDECONFIGURATION = "[Config].[usp_SystemWideConfigurationSave]";
        public const string SELECTSYSTEMWIDECONFIGURATION = "[Config].[usp_SystemWideConfigurationSelect]";



        /// <summary>
        /// [Master].[YardMap]
        /// </summary>

        public const string DELETEYARDMAP = "[Master].[usp_YardMapDelete]";
        public const string LISTYARDMAP = "[Master].[usp_YardMapList]";
        public const string SAVEYARDMAP = "[Master].[usp_YardMapSave]";
        public const string SELECTYARDMAP = "[Master].[usp_YardMapSelect]";


        /// <summary>
        /// [Master].[ContainerInYard]
        /// </summary>

        public const string DELETECONTAINERINYARD = "[Master].[usp_ContainerInYardDelete]";
        public const string LISTCONTAINERINYARD = "[Master].[usp_ContainerInYardList]";
        public const string SAVECONTAINERINYARD = "[Master].[usp_ContainerInYardSave]";
        public const string SELECTCONTAINERINYARD = "[Master].[usp_ContainerInYardSelect]";
        public const string SUGGESTCONTAINERINYARD = "[Master].[usp_SuggestContainerInYard]";



    }
}
